import { useState, useEffect } from 'react'
import './App.css'

function App() {
  const [timer, setTimer] = useState(0)
  const [minutes, setMinutes] = useState(0)

  useEffect(() => {
    const timeout = setTimeout(() => {setTimer((timer) => timer + 1)},1000)
    if (timer % 60 === 0 && timer > 0) {
      setMinutes((minutes) => minutes + 1)
      setTimer(0)
    }
    return () => clearTimeout(timeout)
  }, [timer])

  const resetTimers = () => {
    setTimer(0)
    setMinutes(0)
  }

  return (
    <>
      <h1>Timer</h1>
      <div className="timer">
      minutes: {minutes} seconds: {timer}
      <button onClick={() => resetTimers()}>reset</button>
      </div>
    </>
  )
}

export default App