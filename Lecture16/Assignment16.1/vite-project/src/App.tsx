import { useState, ChangeEvent } from 'react'
import './App.css'

function App() {
  const [phoneNumbers, setPhoneNumbers] = useState<string[]>([])
  
  const submitNumber = (phoneNumber: string) => {
    setPhoneNumbers([...phoneNumbers, phoneNumber])
  }

  return (
    <div className="phoneNumberApp">
      <h1>Phone Numbers</h1>
      <InputField addNumber={submitNumber} />
      <div className="numberList">{phoneNumbers.map((number, i) => {
          return (<div key={"numbers" + i}>
            {number}
          </div>)
        })}
      </div>
    </div>
  )
}

interface Props {
  addNumber: (phoneNumber: string) => void
}

function InputField(props: Props) {
  const [inputNumber, setInputNumber] = useState("")

  const onInputNumberChange = (event: ChangeEvent<HTMLInputElement>) => {
    if (!isNaN(Number(event.target.value))){
      if(inputNumber.length >= 10) {
        props.addNumber(inputNumber)
        setInputNumber("")
      } else {
        setInputNumber(event.target.value)
      }
    }
  }

  return(
    <input className="inputPhoneNumberField" value={inputNumber}
      onChange={onInputNumberChange} />
  )
}

export default App
