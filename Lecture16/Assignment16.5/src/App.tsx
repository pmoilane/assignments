import { useState } from 'react'
import './App.css'

function App() {
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [text, setText] = useState('')
  const [selected, setSelected] = useState('')


  const Send = () => {
    console.log(name, email, text, selected)
  }

  const Reset = () => {
    setName('')
    setEmail('')
    setSelected('')
    setText('')
  }

  const onClick = (value: string) => {
    console.log(value)
    setSelected(value)
  }

  return (
    <>
    
      <div>
          <input type="text" value={name} onChange={e => { setName(e.target.value)}} placeholder='Name'></input>
          
      </div>
      <div>
        <input type="text" value={email} onChange={e => { setEmail(e.target.value)}} placeholder='Email'></input>
      </div>

      <div>
        <input type="radio" value='Feedback' onChange={() => onClick('Feedback')} checked={selected === 'Feedback'} />
        <label>Feedback</label>
      </div>
      <div>
        <input type="radio" value='Suqqestion' onChange={() => onClick('Suggestion')} checked={selected === 'Suggestion'}  />
        <label>Suqqestion</label>
      </div>
      <div>
        <input type="radio" value='Question' onChange={() => onClick('Question')} checked={selected === 'Question'} />
        <label>Question</label>
      </div>

      
      <textarea value={text} onChange={e => { setText(e.target.value)}}>

      </textarea>
      <div>
        <button onClick={() => Send()}>Submit</button>
        <button onClick={() => Reset()}>Reset</button>
      </div>
    
    </>
  )
}

export default App
