import { useState } from 'react'
import './App.css'

interface Contact {
  name: string
  email: string
  phoneNumber: string
  address: string
  website: string
  notes: string
}

interface ActiveView {
  view: string
  index: number
}

interface Props {
  contactList: Array<Contact>
  setContactList: React.Dispatch<React.SetStateAction<Contact[]>>
  activeView: ActiveView
  setActiveView: React.Dispatch<React.SetStateAction<ActiveView>>
}

const starterList = [
  {
    name: "Seppo Taalasmaa",
    email: "Seppo@luukku.com",
    phoneNumber: "0501234567",
    address: "Katu 2",
    website: "seppo.com",
    notes: "important notes"
  },
  {
    name: "Ismo Laitela",
    email: "Ismo@luukku.com",
    phoneNumber: "0501234568",
    address: "Katu 5",
    website: "Ismo.com",
    notes: "important notes are here too"
  }
]

function App() {
  const [contactList, setContactList] = useState<Array<Contact>>(starterList)
  const [searchInput, setSearchInput] = useState("")
  const [activeView, setActiveView] = useState<ActiveView>({view: "", index: NaN})

  const onListClick = (i: number) => {
    setActiveView({ view: "displayContact", index: i})
    console.log("clicked on " + i)
  }

  const viewSwitcher = () => {
    if (activeView.view === "addContact") {
      return <AddContact setContactList={setContactList} contactList={contactList} setActiveView={setActiveView} activeView={activeView}/>
    } else if (activeView.view === "displayContact") {
      return <DisplayContact contactList={contactList} activeView={activeView} setActiveView={setActiveView} setContactList={setContactList} />
    } else if (activeView.view === "editContact") {
      return <EditContact setContactList={setContactList} contactList={contactList} setActiveView={setActiveView} activeView={activeView}/>
    }  else {
      return <h2>Contact Manager</h2>
    }
  }

  return (
    <div className="contact-list-app"> 
      <div className="left-column">
        <input type="text" value={searchInput} onChange={e => setSearchInput(e.target.value)}/>
        <div>{contactList.filter(item => item.name.toLowerCase().includes(searchInput.toLowerCase())).map((contact,i) =>{
          return <div key={"contact" + i} onClick={() => onListClick(i)}>{contact.name}</div> 
        }
        )}</div>
        <button onClick={() => setActiveView({ view:"addContact", index: NaN })}>Add Contact</button>
      </div>
      <div className="right-column">{viewSwitcher()}</div>
    </div>
  )
}

function DisplayContact( {contactList, activeView, setActiveView, setContactList}: Props ) {

  const removeContact = () => {
    const updatedList = contactList.filter((_contact, i) => i !== activeView.index)
    setContactList(updatedList)
    setActiveView({ view: "", index: NaN })
  }

  return (
    <div className="right-column-component">
      <h2>{contactList[activeView.index].name}</h2>
      {contactList[activeView.index].phoneNumber !=="" && <div><i>Phone:</i> {contactList[activeView.index].phoneNumber}</div>}
      {contactList[activeView.index].email !=="" && <div><i>Email:</i> {contactList[activeView.index].email}</div>}
      {contactList[activeView.index].address !=="" && <div><i>Address:</i> {contactList[activeView.index].address}</div>}
      {contactList[activeView.index].website !=="" && <div><i>Website:</i> {contactList[activeView.index].website}</div>}
      {contactList[activeView.index].notes !=="" && <div className="notes"><i>Notes:</i> {contactList[activeView.index].notes}</div>}
      <div>
       <button onClick={() => removeContact()}>Remove</button>
        <button onClick={() => setActiveView({view: "editContact", index: activeView.index})}>Edit</button> 
      </div>
    </div>
  )
}

function AddContact( {setContactList, contactList, setActiveView}: Props ) {
  const [name, setName] = useState("")
  const [email, setEmail] = useState("")
  const [phoneNumber, setPhoneNumber] = useState("")
  const [address, setAddress] = useState("")
  const [website, setWebsite] = useState("")
  const [notes, setNotes] = useState("")

  const addNewContact = () => {
    setContactList([ ...contactList ,{
      name: name,
      email: email,
      phoneNumber: phoneNumber,
      address: address,
      website: website,
      notes: notes
    }])
    resetFields()
  }

  const cancelButton = () => {
    resetFields()
  }

  const resetFields = () => {
    setName("")
    setEmail("")
    setPhoneNumber("")
    setAddress("")
    setWebsite("")
    setNotes("")
    setActiveView({ view: "", index: NaN })
  }

  return (
    <>
      <div className="right-column-component">
        <h2>Add a contact</h2>
        <label>Name</label>
        <input type="text" name="name" value={name} onChange={e => { setName(e.target.value)}} />
        <label>Email address</label>
        <input type="text" name="email" value={email} onChange={e => { setEmail(e.target.value)}} />
        <label>Phone number</label>
        <input type="number" name="phoneNumber" value={phoneNumber} onChange={e => { setPhoneNumber(e.target.value)}} />
        <label>Address</label>
        <input type="text" name="address" value={address} onChange={e => { setAddress(e.target.value)}} />
        <label>Website</label>
        <input type="text" name="website" value={website} onChange={e => { setWebsite(e.target.value)}} />
        <label>Notes</label>
        <textarea name="notes" value={notes} onChange={e => { setNotes(e.target.value)}} />
        <div>
         <button onClick={() => addNewContact()}>
          Save
        </button>
        <button onClick={() => cancelButton()}>
          Cancel
        </button> 
        </div>
      </div>
    </>
  )
}

function EditContact( {setContactList, contactList, setActiveView, activeView}: Props ) {
  const [name, setName] = useState(contactList[activeView.index].name)
  const [email, setEmail] = useState(contactList[activeView.index].email)
  const [phoneNumber, setPhoneNumber] = useState(contactList[activeView.index].phoneNumber)
  const [address, setAddress] = useState(contactList[activeView.index].address)
  const [website, setWebsite] = useState(contactList[activeView.index].website)
  const [notes, setNotes] = useState(contactList[activeView.index].notes)

  const editContact = () => {
    const updatedList = contactList.map((contact, i) => {
      if (i === activeView.index) {
        return contact =
          {
            name: name,
            email: email,
            phoneNumber: phoneNumber,
            address: address,
            website: website,
            notes: notes
          }
        } else {
          return contact
        }
    })
    setContactList(updatedList)
    resetFields()
  }

  const cancelButton = () => {
    resetFields()
  }

  const resetFields = () => {
    setName("")
    setEmail("")
    setPhoneNumber("")
    setAddress("")
    setWebsite("")
    setNotes("")
    setActiveView({view: "", index: NaN})
  }

  return (
    <>
      <div className="right-column-component">
        <h2>Add a contact</h2>
        <div>
          <label>Name</label>
          <input type="text" name="name" value={name} onChange={e => { setName(e.target.value)}} />
        </div>
        <div>
          <label>Email address</label>
          <input type="text" name="email" value={email} onChange={e => { setEmail(e.target.value)}} />
        </div>
        <div>
          <label>Phone number</label>
          <input type="number" name="phoneNumber" value={phoneNumber} onChange={e => { setPhoneNumber(e.target.value)}} />
        </div>
        <div>
          <label>Address</label>
          <input type="text" name="address" value={address} onChange={e => { setAddress(e.target.value)}} />
        </div>
        <div>
          <label>Website</label>
          <input type="text" name="website" value={website} onChange={e => { setWebsite(e.target.value)}} />
        </div>
        <div>
          <label>Notes</label>
          <textarea name="notes" value={notes} onChange={e => { setNotes(e.target.value)}} />
        </div>
        <div>
          <button onClick={() => editContact()}>
            Save
          </button>
          <button onClick={() => cancelButton()}>
            Cancel
          </button>
          </div>
      </div>
    </>
  )
}

export default App
