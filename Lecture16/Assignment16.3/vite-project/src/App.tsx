/* eslint-disable react-hooks/exhaustive-deps */
import { useState, useEffect } from 'react'
import './App.css'
import axios from 'axios'

function App() {
  const [joke, setJoke] = useState("")

  const url = "https://api.api-ninjas.com/v1/dadjokes"
  const config = {
    headers: {
      "X-Api-Key" : "qFRwfcoEjiLRvRYhJirWtQ==1JTjQy3ah2MoAPgM"
    }
  }

  useEffect(() => {
    async function fetchData() {
      const response = await axios.get(url, config)
      const joke = response.data[0]
      setJoke(joke.joke)
    }
    fetchData()
  }, [])

  return (
    <>
      <h1>Dad Jokes</h1>
      <div className="DadJoke">
        {joke}
      </div>
    </>
  )
}

export default App
