import { useState, ChangeEvent, useEffect } from 'react'
import './App.css'
import axios from "axios"

function App() {
  const [count, setCount] = useState(0)
  const [catText, setCatText] = useState("")
  const [catPicture, setCatPicture] = useState("https://cataas.com/cat")

  const onCatTextChange = (event: ChangeEvent<HTMLInputElement>) => {
    setCatText(event.target.value)
  }

  const reloadCatPicture = () => {
    setCount(count => count + 1)
  }

  useEffect(() => {
    if (catText === "") {
      setCatPicture("https://cataas.com/cat")
    } else {
      setCatPicture("https://cataas.com/cat" + "/says/" + catText)
    }
  }, [count])

  return (
    <>
      <h1>Cats</h1>
      <div className="catPictures">
        <div>
         <img src={catPicture + `?key=${count}`}/> 
        </div>
        <input value={catText} onChange={onCatTextChange}/>
        <button onClick={() => reloadCatPicture()}>
          Reload
        </button>
      </div>
    </>
  )
}

export default App
