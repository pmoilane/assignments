import { useLoaderData } from "react-router-dom"
import songs from "./songList"
import { Song } from "./songList"

export const loader = ({ params }: any) => {
  const id = params.id
  console.log(songs.find(item => item.id === Number(id)))
  if (songs.find(item => item.id === Number(id)) !== undefined) {
    return id
  }
  throw new Response(`Song with Id:${id} doesn't exist`, { status: 404 })
}

export default function Songs() {
  const id = Number(useLoaderData() as string)
  const song: Song = songs.filter(item => item.id === id)[0]
  return (
    <div className='Song'>
      <h1>{song.title}</h1>
      <div>{song.lyrics}</div>
    </div>
  )
}