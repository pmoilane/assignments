import { Link, Outlet } from "react-router-dom"
import songs from "./songList"

function Root() {
  return(
    <div className='Root'>
      <h1>Song List</h1>
      <nav>
        {songs.map(item => {return (
          <div>
            <Link to={`/songs/${item.id}`}>{item.title}</Link>
          </div>
        )})}
      </nav>
        <Outlet />
    </div>
  )
}

export default Root