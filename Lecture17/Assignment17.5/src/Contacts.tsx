import { useLoaderData } from "react-router-dom"

interface Contact {
  id: number
  name: string
  phone: string
  email: string
}

const contactList: Array<Contact> = [
  {
    id: 1,
    name: "Kalle Kalastaja",
    phone: "0502345678",
    email: "Kalle@luukku.com"
  },
  {
    id: 2,
    name: "Erkki Esimerkki",
    phone: "0501234567",
    email: "Erkki@luukku.com"
  },
  {
    id: 3,
    name: "Antti Autoilija",
    phone: "0503456789",
    email: "Antti@luukku.com"
  }
]

export const loader = ({ params }: any) => {
  return params.id
}

export default function Contacts() {
  const id = Number(useLoaderData() as string)
  const contact: Contact = contactList.filter(item => item.id === id)[0]
  return (
    <div className='Contacts'>
      <h1>Contact</h1>
      <div>Name: {contact.name}</div>
      <div>Phone: {contact.phone}</div>
      <div>Email: {contact.email}</div>
    </div>
  )
}