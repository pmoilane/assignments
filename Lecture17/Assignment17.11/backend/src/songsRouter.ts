import express, {Request, Response} from "express"
import { readAllSongs, readSong } from "./dao"

const songsRouter = express.Router()

songsRouter.get("/", async (req: Request, res: Response) => {
	const result = await readAllSongs()
	res.send(result)
})

songsRouter.get("/:id", async (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const product = await readSong(id)
	res.send(product)
})

export default songsRouter

