import express, { Request, Response } from "express"
import songsRouter from "./songsRouter"
//import { createSongsTable } from "./db"
import "dotenv/config"

const server = express()

//createSongsTable()

server.use("/", express.static("./dist/client"))

server.use("/api/songs", songsRouter)

server.get("*", (req: Request, res: Response) => {
	res.sendFile("index.html", { root: "./dist/client" })
})

server.listen(3000, () => {
	console.log("Listening to port 3000")
})