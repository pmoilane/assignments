// db.ts
import pg from "pg"
import songs from "./songList"
import { addSong } from "./dao"
import "dotenv/config"

const { PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE } = process.env

export const pool = new pg.Pool({
	host: PG_HOST,
	port: Number(PG_PORT),
	user: PG_USERNAME,
	password: PG_PASSWORD,
	database: PG_DATABASE
})

export const executeQuery = async (query: string, parameters?: Array<any>) => {
	const client = await pool.connect()
	try {
		const result = await client.query(query, parameters)
		return result
	} catch (error: any) {
		console.error(error.stack)
		error.name = "dbError"
		throw error
	} finally {
		client.release()
	}
}

export const createSongsTable = async () => {
	const query = `
        CREATE TABLE IF NOT EXISTS "songs" (
            "id" SERIAL PRIMARY KEY,
            "title" VARCHAR(100) NOT NULL,
            "lyrics" VARCHAR NOT NULL
        )`
	await executeQuery(query)
	console.log("Songs table initialized")
	for (let i = 0; i < songs.length; i++) {
		addSong(songs[i].id, songs[i].title, songs[i].lyrics)
	}
	console.log("Songs added")
}
