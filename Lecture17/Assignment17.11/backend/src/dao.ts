import { executeQuery } from "./db"

export const readSong = async (id: number) => {
	const query = "SELECT * FROM songs WHERE id = $1"
	const params = [id]
	const result = await executeQuery(query, params)
	return result.rows[0]
}

export const readAllSongs = async () => {
	const query = "SELECT * FROM songs"
	const result = await executeQuery(query)
	return result.rows
}

export const addSong = async (id: number, title: string, lyrics: string) => {
	const query = "INSERT INTO songs (id, title, lyrics) VALUES ($1, $2, $3) RETURNING id"
	const params = [id, title, lyrics]
	const result = await executeQuery(query, params)
	return result.rows[0].id
}