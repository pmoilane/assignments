"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const songList_1 = __importDefault(require("./songList"));
const songsRouter = express_1.default.Router();
songsRouter.get("/", (req, res) => {
    const songList = songList_1.default.map(item => { return { id: item.id, title: item.title }; });
    res.send(songList);
});
songsRouter.get("/:id", (req, res) => {
    const songInfo = songList_1.default.find(item => item.id === Number(req.params.id));
    if (songInfo !== undefined) {
        res.send(songInfo);
    }
    else {
        res.status(404).send("Unknown song ID");
    }
});
exports.default = songsRouter;
