import express, { Request, Response } from "express"

const server = express()

server.use("/", express.static("./dist/client"))

server.get("*", (req: Request, res: Response) => {
	res.sendFile("index.html", { root: "./dist/client" })
})

server.listen(3000, () => {
	console.log("Listening to port 3000")
})