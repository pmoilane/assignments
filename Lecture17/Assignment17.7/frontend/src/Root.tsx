import { Link, Outlet } from "react-router-dom"

function Root() {
  return(
    <div className='Root'>
      <h1>Contacts</h1>
      <nav>
        <Link to={"/contact/1"}>Kalle</Link> _
        <Link to={"/contact/2"}>Erkki</Link> _
        <Link to={"/contact/3"}>Antti</Link>
      </nav>
        <Outlet />
    </div>
  )
}

export default Root