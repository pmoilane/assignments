import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom'

function Root() {
  return(
    <div className='Root'>root</div>
  )
}

function Contacts() {
  return(
    <div className='Contacts'>contacts</div>
  )
}

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root/>
  },
  {
    path: "/contacts",
    element: <Contacts/>
  }
])

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
