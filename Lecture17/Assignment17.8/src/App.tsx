import { CSSProperties, useState } from "react"

function App() {
  const [color, setColor] = useState(Math.floor(Math.random()*16777215).toString(16))

  const randomColor = () => {
    setColor(Math.floor(Math.random()*16777215).toString(16))
  }

  const container: CSSProperties = {
    display: "flex",
    flexDirection: "column",
    flexWrap: "nowrap",
    justifyContent: "center",
    alignItems: "center",
    alignContent:" stretch",
    height: "100vh"
  }

  const item: CSSProperties = {
    display: "block",
    flexGrow: 0,
    flexShrink: 1,
    flexBasis: "auto",
    alignSelf: "auto",
    order: 0,
    padding: "24px",
  }

  return (
    <>
      <div style={container}>
        <div style={item}>{`#${color}`}</div>
        <div style={ {...item, backgroundColor: `#${color}`}} onClick={() => randomColor()}> 
        </div>
      </div>
    </>
      
  )
}

export default App
