import express, {Request, Response} from "express"
import songs from "./songList"

const songsRouter = express.Router()

songsRouter.get("/", (req: Request, res: Response) => {
	const songList = songs.map(item => {return { id: item.id, title: item.title }})
	res.send(songList)
})

songsRouter.get("/:id", (req: Request, res: Response) => {
	const songInfo = songs.find(item => item.id === Number(req.params.id))
	if (songInfo !== undefined) {
		res.send(songInfo)
	} else {
		res.status(404).send("Unknown song ID")
	}
})

export default songsRouter