"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const songsRouter_1 = __importDefault(require("./songsRouter"));
const server = (0, express_1.default)();
server.use("/", express_1.default.static("./dist/client"));
server.use("/api/songs", songsRouter_1.default);
server.get("*", (req, res) => {
    res.sendFile("index.html", { root: "./dist/client" });
});
server.listen(3000, () => {
    console.log("Listening to port 3000");
});
