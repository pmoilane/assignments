import { useLoaderData } from "react-router-dom"
import { useState, useEffect } from "react"

export interface Song {
  id: number
  title: string
  lyrics: string
}

export const loader = ({ params }: any) => {
  const songids = [1, 2, 3, 4, 5]
  const id = params.id
  if (songids.find(item => item === Number(id)) !== undefined) {
    return id
  }
  throw new Response(`Song with Id:${id} doesn't exist`, { status: 404 })
}

export default function Songs() {
  const [songInfo, setSongInfo] = useState<Song>({id: NaN, title: "", lyrics: ""})

  const id = Number(useLoaderData() as string)

  useEffect(() => {
    const getSongList = () => {
      fetch(`http://localhost:3000/api/songs/${id}`)
        .then(response => {
          return response.json()
        })
        .then(data => {
          setSongInfo(data)
        })
    }
    getSongList()
  }, [id])

  return (
    <div className='Song'>
      <h1>{songInfo.title}</h1>
      <div>{songInfo.lyrics}</div>
    </div>
  )
}