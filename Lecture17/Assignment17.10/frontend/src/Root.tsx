import { Link, Outlet } from "react-router-dom"
import { useEffect, useState } from "react"

interface SongList {
  id: number
  title: string
}

function Root() {
  const [songList, setSongList] = useState<Array<SongList>>([{id: NaN, title: ""}])

  const url = "http://localhost:3000/api/songs/"

  useEffect(() => {
    const getSongList = () => {
      fetch(url)
        .then(response => {
          return response.json()
        })
        .then(data => {
          console.log(data)
          setSongList(data)
        })
    }
    getSongList()
  }, [])

  return(
    <div className='Root'>
      <h1>Song List</h1>
      <nav>
        {songList.map(item => {return (
          <div>
            <Link to={`/songs/${item.id}`}>{item.title}</Link>
          </div>
        )})}
      </nav>
        <Outlet />
    </div>
  )
}

export default Root