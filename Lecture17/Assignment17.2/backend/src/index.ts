import express, { Request, Response } from "express"

const server = express()

server.use("/", express.static("./dist/client"))

server.get("/version", (_req: Request, res: Response) => {
	res.send("1.0")
})

server.listen(3000, () => {
	console.log("Listening to port 3000")
})