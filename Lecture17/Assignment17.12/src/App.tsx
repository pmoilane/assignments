//import './App.css'
import MouseEvent, { useState } from "react"

interface Coordinates {
  x: number
  y: number
}

function App() {
  const [mouseState, setMouseState] =  useState("up")
  const [coordinates, setCoordinates] = useState<Coordinates>({ x: window.innerWidth / 2, y: window.innerHeight / 2})

  const mouseMove = (event: MouseEvent.MouseEvent<HTMLDivElement, MouseEvent>) => {
    if (mouseState === "down"){
      setCoordinates({ x: event.pageX, y: event.pageY })
    }
  }

  return (
    <div style={
      { 
        position: "absolute",
        top: coordinates.y-25,
        left: coordinates.x-25,
        padding: "20px",
        background: "#d40000"
      }
      }onMouseDown={() => {setMouseState("down")}} onMouseMove={(e) => {mouseMove(e)}} onMouseUp={() => {setMouseState("up")}}>
      box
    </div>
  )
}

export default App
