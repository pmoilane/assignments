//8.11

import express, { Request, Response} from "express"
import { unknownEndpoint } from "./middlewares"

const server = express()
server.use(express.json())

const books: Array<{id: number,name: string, author: string, read: boolean}> = []

server.get("/api/v1/books", (req: Request, res: Response) => {
	res.send(books)
})

server.get("/api/v1/books/:id", (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const info = books.find(item => item.id === id)
	if (info === undefined) {
		res.status(404).send("ERROR, book id not found")
	} else {
		res.send(info)
	}
})

server.post("/api/v1/books", (req: Request, res: Response) => {
	const book = req.body
	if (typeof book.id !== "number" || typeof book.name !== "string" || typeof book.author !== "string" || typeof book.read !== "boolean") {
		res.status(400).send("ERROR, missing parameter or wrong type")
	} else {
		const bookInfo = {id: book.id, name: book.name, author: book.author, read: book.read}
		books.push(bookInfo)
		res.status(201).send()
	}
})

server.put("/api/v1/books/:id", (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const book = req.body
	const index = books.findIndex(item => item.id === id)
	if (index === -1) {
		res.status(404).send("ERROR, book id not found")
	} else {
		if (typeof book.name !== "string" && typeof book.author !== "string" && typeof book.read !== "boolean") {
			res.status(400).send("ERROR, incorrect parameters or parameter type")
		} else {
			if (typeof book.name === "string") {
				books[index].name = book.name
			}
			if (typeof book.author === "string") {
				books[index].author = book.author
			}
			if (typeof book.read === "boolean") {
				books[index].read = book.read
			}
			res.status(204).send()
		}
	}
})

server.delete("/api/v1/books/:id", (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const index = books.findIndex(item => item.id === id)
	if (index === -1) {
		res.status(404).send("ERROR, book id not found")
	} else {
		books.splice(index, 1)
		res.status(204).send()
	}
})

server.use(unknownEndpoint)

server.listen(3002)
