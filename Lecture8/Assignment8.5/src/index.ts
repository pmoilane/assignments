//8-5

import express, { Request, Response, NextFunction} from "express"

const server = express()


const logger = (req: Request, res: Response, next: NextFunction) => {
	const now = new Date()
	const method = req.method
	const url = req.url
	console.log(now.toLocaleString(), method, url)
	next()
}

server.use(logger)

server.get("/students", (req: Request, res: Response) => {
	res.send([])
})

server.listen(3002)

