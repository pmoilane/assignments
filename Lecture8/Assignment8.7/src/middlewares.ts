import { Request, Response, NextFunction} from "express"

export const logger = (req: Request, res: Response, next: NextFunction) => {
	const now = new Date()
	const method = req.method
	const url = req.url
	const body = req.body
	if (!body) {
		console.log(now.toLocaleString(), method, url)
	} else {
		console.log(now.toLocaleString(), method, url, body)
	}
	next()
}

export const unknownEndpoint = (_req, res) => {
	res.status(404).send("ERROR, Endpoint not valid")
}
