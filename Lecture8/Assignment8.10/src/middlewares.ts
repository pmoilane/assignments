import { Request, Response, NextFunction} from "express"

export const checkInput = (req: Request, res: Response, next: NextFunction) => {
	const body = req.body
	console.log(Array.isArray(body))
	if (Array.isArray(body)) {
		for (let i = 0; i <= body.length; i++) {
			if (i === body.length) {
				next()
				break
			}
			if (typeof body[i]["score"] === "number" && typeof body[i]["name"] === "string") {
				continue
			} else {
				res.status(400).send("request body is not of correct type")
				break
			}
		}
	} else {
		res.status(400).send("request body is not of correct type")
	}
}

export const unknownEndpoint = (_req: Request, res: Response) => {
	res.status(404).send("ERROR, Endpoint not valid")
}
