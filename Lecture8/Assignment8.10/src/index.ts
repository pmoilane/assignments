// 8.10

import express, { Request, Response} from "express"
import { checkInput, unknownEndpoint } from "./middlewares"

const server = express()
server.use(express.json())

server.use(checkInput)

server.get("/scores", (req: Request, res: Response) => {
	const students = req.body
	const highestScoringStudent = highestScore(students)
	const scoreAverage = averageScore(students)
	const aboveAveragePercentage = aboveAverageStudents(students) / students.length * 100
	const responseJSON = { "highestScoringStudent": highestScoringStudent, "averageScore": scoreAverage, "aboveAveragePercentage": aboveAveragePercentage}
	console.log(responseJSON)
	res.status(201).send(responseJSON)
})

server.use(unknownEndpoint)

server.listen(3000)

function highestScore(students: Array<{name: string, score: number}>): string {
	let highestScore = -Infinity
	let highestStudent = ""
	for (let index = 0; index < students.length; index++) {
		if (students[index].score > highestScore) {
			highestScore = students[index].score
			highestStudent = students[index].name
		}
	}
	return highestStudent
}

function averageScore(students: Array<{name: string, score: number}>): number {
	return students.reduce((acc, cur) => acc + cur.score, 0) / students.length
}

function aboveAverageStudents(students: Array<{name: string, score: number}>): number {
	let result = 0
	const average = averageScore(students)
	for (let index = 0; index < students.length; index++) {
		if (students[index].score >= average) {
			result += 1
		}
	}
	return result
}