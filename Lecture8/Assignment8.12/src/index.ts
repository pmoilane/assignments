//8.12

import express, { NextFunction, Request, Response} from "express"
import { checkInput, unknownEndpoint, logger, logReader } from "./middlewares"
import helmet from "helmet"

const server = express()
server.use(helmet())
server.use(express.json())
server.use(logger)

const books: Array<{id: number,name: string, author: string, read: boolean}> = []

server.get("/api/v1/books", (req: Request, res: Response) => {
	res.send(books)
})

server.get("/api/v1/books/logs", (req: Request, res: Response) => {
	logReader(req, res)
})

server.get("/api/v1/books/:id", (req: Request, res: Response, next: NextFunction) => {
	const id = Number(req.params.id)
	const info = books.find(item => item.id === id)
	if (info === undefined) {
		const error = new Error("Book id not found")
		next(error)
	} else {
		res.send(info)
	}
})

server.use(checkInput)

server.post("/api/v1/books", (req: Request, res: Response) => {
	const book = req.body
	books.push(book)
	res.status(201).send()
})

server.put("/api/v1/books/:id", (req: Request, res: Response, next: NextFunction) => {
	const id = Number(req.params.id)
	const book = req.body
	const index = books.findIndex(item => item.id === id)
	if (index === -1) {
		const error = new Error("Book id not found")
		next(error)
	} else {
		if (typeof book.name === "string") {
			books[index].name = book.name
		}
		if (typeof book.author === "string") {
			books[index].author = book.author
		}
		if (typeof book.read === "boolean") {
			books[index].read = book.read
		}
		res.status(204).send()
	}
})

server.delete("/api/v1/books/:id", (req: Request, res: Response, next: NextFunction) => {
	const id = Number(req.params.id)
	const index = books.findIndex(item => item.id === id)
	if (index === -1) {
		const error = new Error("Id not found")
		next(error)
	} else {
		books.splice(index, 1)
		res.status(204).send()
	}
})

const errorHandler = (error: Error, req: Request, res: Response, next: NextFunction) => {
	res.status(404).send(error.message)
	next(error)
}

server.use(unknownEndpoint)
server.use(errorHandler)
server.listen(3002)
