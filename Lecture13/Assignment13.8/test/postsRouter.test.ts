/* eslint-disable @typescript-eslint/no-explicit-any */
import request from "supertest"
import server from "../src/server"
import { pool } from "../src/db"

const initializeMockPool = (mockResponse: any) => {
	(pool as any).connect = jest.fn(() => {
		return {
			query: () => mockResponse,
			release: () => null
		}
	})
}

describe("Testing GET /posts", () => {
	const mockResponse = {
		rows: [
			{ id: 101, user_id: 101, title: "Test title 1" },
			{ id: 102, user_id: 102, title: "Test title 2" }
		]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})
	
	// the actual tests
	it("Returns 200 with all posts", async () => {
		const response = await request(server).get("/posts")
		expect(response.statusCode).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows)
	})
})

describe("Testing GET /posts/:id", () => {
	const mockResponse = {
		rows: [
			{ id: 101, user_id: 101, title: "Test title 1", content: "Test text", date: `${Date.now()}`, post_id: 101 } 
		]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})
	
	// the actual tests
	it("Returns 200 with all posts", async () => {
		const response = await request(server).get("/posts/101")
		expect(response.statusCode).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows)
	})
})

describe("Testing POST /posts", () => {
	const mockResponse = {
		rows: [
			{ id: 1, userId: 101, title: "Test post", content: "Some test text" }
		]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})
	
	// the actual tests
	it("Returns 200 with a post", async () => {
		const response = await request(server)
			.post("/posts")
			.send({ userId: 101, title: "Test post", content: "Some test text" })
		expect(response.statusCode).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows[0])
	})
})

describe("Testing DELETE /posts/:id", () => {
	const mockResponse = {
		rows: [
			{ response: "Deleted post with id: 101" }
		]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})
	
	// the actual tests
	it("Returns 200 with response text with id of deleted post", async () => {
		const response = await request(server)
			.delete("/posts/101")
		expect(response.statusCode).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows[0])
	})
})