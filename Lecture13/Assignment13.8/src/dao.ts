import { executeQuery } from "./db"

export const readAllUsers = async () => {
	const query = "SELECT id, username FROM users"
	const result = await executeQuery(query)
	return result.rows
}

export const readUser = async (id: number) => {
	const query = "SELECT * FROM users WHERE id = $1"
	const params = [id]
	const result = await executeQuery(query, params)
	return result.rows[0]
}

export const addUser = async (username: string, full_name: string, email: string) => {
	const query = "INSERT INTO users (username, full_name, email) VALUES ($1, $2, $3) RETURNING id"
	const params = [username, full_name, email]
	const result = await executeQuery(query, params)
	return result.rows[0].id
}

export const deleteUser = async (id: number) => {
	const query = "DELETE FROM users WHERE id = $1"
	const params = [id]
	const result = await executeQuery(query, params)
	return result.rows
}

export const readAllPosts = async () => {
	const query = "SELECT id, user_id, title FROM posts"
	const result = await executeQuery(query)
	return result.rows
}

export const readPost = async (id: number) => {
	const query = "SELECT * FROM posts JOIN comments ON posts.id = comments.post_id WHERE posts.id = $1"
	const params = [id]
	const result = await executeQuery(query, params)
	return result.rows
}

export const addPost = async (user_id: number, title: string, content: string) => {
	const query = "INSERT INTO posts (user_id, title, content, date) VALUES ($1, $2, $3, NOW()) RETURNING id"
	const params = [user_id, title, content]
	const result = await executeQuery(query, params)
	return result.rows[0].id
}

export const deletePost = async (id: number) => {
	const query = "DELETE FROM posts WHERE id = $1"
	const params = [id]
	const result = await executeQuery(query, params)
	return result.rows
}

export const readComments = async (id: number) => {
	const query = "SELECT * FROM comments WHERE user_id = $1"
	const params = [id]
	const result = await executeQuery(query, params)
	return result.rows
}

export const addComment = async (user_id: number, post_id: number, content: string) => {
	const query = "INSERT INTO comments (user_id, post_id, content, date) VALUES ($1, $2, $3, NOW()) RETURNING id"
	const params = [user_id, post_id, content]
	const result = await executeQuery(query, params)
	return result.rows[0].id
}

export const deleteComment = async (id: number) => {
	const query = "DELETE FROM comments WHERE id = $1"
	const params = [id]
	const result = await executeQuery(query, params)
	return result.rows
}