/* eslint-disable @typescript-eslint/no-explicit-any */
import request from "supertest"
import server from "../src/server"
import { pool } from "../src/db"

const initializeMockPool = (mockResponse: any) => {
	(pool as any).connect = jest.fn(() => {
		return {
			query: () => mockResponse,
			release: () => null
		}
	})
}

describe("Testing GET /comments/:id", () => {
	const mockResponse = {
		rows: [
			{ id: 101, user_id: 101, post_id: 101, content: "Test comment", date: `${Date.now()}` } 
		]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})
	
	// the actual tests
	it("Returns 200 with all comments by user with id 101", async () => {
		const response = await request(server).get("/comments/101")
		expect(response.statusCode).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows)
	})
})

describe("Testing POST /comments", () => {
	const mockResponse = {
		rows: [
			{ id: 101, userId: 101, postId: 101, content: "Some test comment" }
		]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})
	
	// the actual tests
	it("Returns 200 with a comment", async () => {
		const response = await request(server)
			.post("/comments")
			.send({ userId: 101, postId: 101, content: "Some test comment" })
		expect(response.statusCode).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows[0])
	})
})

describe("Testing DELETE /comments/:id", () => {
	const mockResponse = {
		rows: [
			{ response: "Deleted comment with id: 101" }
		]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})
	
	// the actual tests
	it("Returns 200 with response text with id of deleted comment", async () => {
		const response = await request(server)
			.delete("/comments/101")
		expect(response.statusCode).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows[0])
	})
})