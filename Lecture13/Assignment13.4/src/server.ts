import express from "express"
//import { createProductsTable } from "./db"
import router from "./productsRouter"

const server = express()
server.use(express.json())

//createProductsTable()

server.use("/products", router)

export default server