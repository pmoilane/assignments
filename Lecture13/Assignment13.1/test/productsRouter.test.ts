/* eslint-disable @typescript-eslint/no-explicit-any */
import request from "supertest"
import server from "../src/server"
import { pool } from "../src/db"

const initializeMockPool = (mockResponse: any) => {
	(pool as any).connect = jest.fn(() => {
		return {
			query: () => mockResponse,
			release: () => null
		}
	})
}

describe("Testing GET /products", () => {
	const mockResponse = {
		rows: [
			{ id: 101, name: "Test Item 1", price: 100 },
			{ id: 102, name: "Test Item 2", price: 200 }
		]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})
	
	// the actual tests
	it("Returns 200 with all products", async () => {
		const response = await request(server).get("/products")
		expect(response.statusCode).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows)
	})
})

describe("Testing GET /products/:id", () => {
	const mockResponse = {
		rows: [
			{ id: 101, name: "Test Item 1", price: 100 }
		]
	}

	beforeAll(() => {
		initializeMockPool(mockResponse)
	})

	afterAll(() => {
		jest.clearAllMocks()
	})
	
	// the actual tests
	it("Returns 200 with a product", async () => {
		const response = await request(server).get("/products/101")
		expect(response.statusCode).toBe(200)
		expect(response.body).toStrictEqual(mockResponse.rows[0])
	})
})