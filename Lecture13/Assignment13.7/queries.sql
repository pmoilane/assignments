CREATE TABLE "users" (
  "id" SERIAL PRIMARY KEY,
  "username" varchar UNIQUE NOT NULL,
  "full_name" varchar NOT NULL,
  "email" varchar UNIQUE NOT NULL
);

CREATE TABLE "posts" (
  "id" SERIAL PRIMARY KEY,
  "user_id" int NOT NULL,
  "title" varchar NOT NULL,
  "content" varchar NOT NULL,
  "date" timestamp NOT NULL,
  CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE "comments" (
  "id" SERIAL PRIMARY KEY,
  "user_id" int NOT NULL,
  "post_id" int NOT NULL,
  "content" varchar NOT NULL,
  "date" timestamp NOT NULL,
  CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES users(id),
  CONSTRAINT fk_post FOREIGN KEY (post_id) REFERENCES posts(id)
);

INSERT INTO users (username, full_name, email) 
VALUES 
    ('Tepe', 'Teppo Testaaja', 'teppo.testaaja@buutti.com'),
    ('Taitsa', 'Taija Testaaja', 'taija.testaaja@buutti.com'),
    ('Outi','Outi Ohjelmoija', 'outi.ohjelmoija@buutti.com'),
    ('Olli', 'Olli Ohjelmoija', 'olli.ohjelmoija@buutti.com'),
    ('Masa', 'Matti Meikäläinen', 'matti.meikalainen@buutti.com');

INSERT INTO posts (user_id, title, content, date)
VALUES
    (1, 'Testi123', 'Tämä on testi', NOW()),
    (2, 'Ilmoitus', 'Tärkeä ilmoitus :D', NOW()),
    (4, 'Testi','Tämäkin on testi', NOW()),
    (5, 'Myydään tietokone', 'hyvin pyörii pelit', NOW()),
    (5, 'Makkara resepti', 'Grillaa makkara, laita sinappia.', NOW());

INSERT INTO comments (user_id, post_id, content, date)
VALUES
    (4, 1, 'hyvin testattu', NOW()),
    (5, 2, ':D', NOW()),
    (1, 3, 'ok', NOW()),
    (2, 4, 'tarjoan 100€', NOW()),
    (2, 5, 'Hyvältä maistuu', NOW());