function Ingredient(name: string, amount: number) {
    this.name = name;
    this.amount = amount;
}

function Recipe(name: string, ingredients: Array<{ name: string, amount: number }>, servings: number) {
    this.name = name;
    this.ingredients = ingredients;
    this.servings = servings;
    this.toString = function() {
        return `${name}, ainesosat: ${ingredients.map((ingredient) => ingredient.name + " " + ingredient.amount)}, ${String(servings)} annosta`;
    };
}

const maito = new Ingredient("maito", 2);
const jauhot = new Ingredient("jauhot", 3);
const voi = new Ingredient("voi", 5);
const sekoitus = new Recipe("sekoitus", [maito, jauhot, voi], 5);

console.log(sekoitus.toString());
