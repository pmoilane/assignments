function generateCredentials(firstname: string, lastname: string) {
    const username = `B${Date().substring(13,15)}${lastname.substring(0,2).toLowerCase()}${firstname.substring(0,2).toLowerCase()}`;
    const password = `${String.fromCharCode(randomNumberGenerator(65,90))}${firstname.split("")[0].toLowerCase()}${lastname.split("")[lastname.length - 1].toUpperCase()}${String.fromCharCode(randomNumberGenerator(33,47))}${Date().substring(13,15)}`;
    return [username, password];
}

function randomNumberGenerator(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

console.log(generateCredentials("John", "Doe"));
console.log(generateCredentials("Jack", "Bauer"));