function checkExam(correctAns: Array<string>, ans: Array<string>) {
    let sum = 0;
    for (let index = 0; index < correctAns.length; index++) {
        if (correctAns[index] === ans[index]) {
            sum += 4;
        } else if (!ans[index]) {
            sum += 0;    
        } else if (correctAns[index] !== ans[index]) {
            sum += -1;
        }
    }
    if (sum < 0) {
        return 0;
    } else {
        return sum;
    }
}

console.log(checkExam(["a", "a", "b", "b"], ["a", "c", "b", "d"])); // → 6 
console.log(checkExam(["a", "a", "c", "b"], ["a", "a", "b",  ""])); // → 7
console.log(checkExam(["a", "a", "b", "c"], ["a", "a", "b", "c"])); // → 16  
console.log(checkExam(["b", "c", "b", "a"], ["",  "a", "a", "c"])); // → 0 
