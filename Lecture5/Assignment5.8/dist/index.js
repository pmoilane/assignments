"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const forecast = JSON.parse(fs_1.default.readFileSync("./forecast.json", "utf-8"));
forecast.temperature = 22;
fs_1.default.writeFileSync("./forecast.json", JSON.stringify(forecast, null, "    "));
