import fs from "fs";

const forecast = JSON.parse(fs.readFileSync("./forecast.json", "utf-8"));
forecast.temperature = 22;
fs.writeFileSync("./forecast.json", JSON.stringify(forecast, null, "    "));
