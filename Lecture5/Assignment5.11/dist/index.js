"use strict";
function aboveAverage(nums) {
    const average = nums.reduce((acc, cur) => acc + cur, 0) / nums.length;
    const result = nums.filter((num) => num > average);
    return result;
}
console.log(aboveAverage([1, 5, 9, 3]));
console.log(aboveAverage([1, 3, 5, 7, 9, 11, 13, 15]));
