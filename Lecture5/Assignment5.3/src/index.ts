function Ingredient(name: string, amount: number) {
    this.name = name;
    this.amount = amount;
}

function Recipe(name: string, ingredients: Array<{ name: string, amount: number }>, servings: number) {
    this.name = name;
    this.ingredients = ingredients;
    this.servings = servings;
}
// 1
Recipe.prototype.toString = function() {
    return `${this.name}, ainesosat: ${this.ingredients.map((ingredient) => ingredient.name + " " + ingredient.amount)}, ${String(this.servings)} annosta`;
};
// 2
Ingredient.prototype.scaleAmount = function(scaleFactor: number) {
    this.amount = this.amount * scaleFactor;
};
// 3 bonus
Recipe.prototype.setServings = function(newServings: number) {
    const scale = newServings / this.servings;
    this.ingredients.forEach((ingredient) => ingredient.scaleAmount(scale));
    //console.log(this.ingredients.forEach((ingredient) => ingredient.number = ingredient.number * scale));
    this.servings = newServings;
};

const maito = new Ingredient("maito", 2);
const jauhot = new Ingredient("jauhot", 3);
const voi = new Ingredient("voi", 5);
const sekoitus = new Recipe("sekoitus", [maito, jauhot, voi], 5);

console.log(sekoitus.toString());
jauhot.scaleAmount(3);
console.log(sekoitus.toString());
sekoitus.setServings(10);
console.log(sekoitus.toString());
