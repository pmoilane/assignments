class Ingredient {
    constructor(name, amount) {
        this.name = name;
        this.amount = amount;
    }
    scaleAmount(scaleFactor) {
        this.amount = this.amount * scaleFactor;
    }
}
class Recipe {
    constructor(name, ingredients, servings) {
        this.name = name;
        this.ingredients = ingredients;
        this.servings = servings;
    }
    toString() {
        return `${this.name}, ainesosat: ${this.ingredients.map((ingredient) => ingredient.name + " " + ingredient.amount)}, ${String(this.servings)} annosta`;
    }
    setServings(newServings) {
        const scale = newServings / this.servings;
        this.ingredients.forEach((ingredient) => { return ingredient.scaleAmount(scale); });
        this.servings = newServings;
    }
}
class HotRecipe extends Recipe {
    constructor(name, ingredients, servings, heatLevel) {
        super(name, ingredients, servings);
        this.heatLevel = heatLevel;
    }
    toString() {
        if (this.heatLevel > 5) {
            return `Warning! This is a Heatlevel ${this.heatLevel}, ` + super.toString();
        }
        else {
            return super.toString();
        }
    }
}
//const maito = new Ingredient("maito", 2);
const jauhot = new Ingredient("jauhot", 3);
const voi = new Ingredient("voi", 5);
//const sekoitus = new Recipe("sekoitus", [maito, jauhot, voi], 5);
const chili = new Ingredient("chili", 1);
const habanero = new Ingredient("habanero", 1);
const mieto = new HotRecipe("mieto", [jauhot, voi, chili], 10, 3);
const tulinen = new HotRecipe("tulinen", [jauhot, voi, habanero], 10, 8);
mieto.setServings(100);
tulinen.setServings(100);
console.log(mieto.toString());
console.log(tulinen.toString());
