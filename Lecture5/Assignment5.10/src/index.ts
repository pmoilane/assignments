function likes(likes: Array<string>) {
    if (likes.length === 0) {
        return "no one likes this";
    } else if (likes.length  === 1) {
        return `${likes[0]} likes this`;
    } else if (likes.length  === 2) {
        return `${likes[0]} and ${likes[1]} like this`;
    } else if (likes.length  === 3) {
        return `${likes[0]}, ${likes[1]} and ${likes[2]} like this`;
    } else if (likes.length >= 3) {
        return `${likes[0]}, ${likes[1]} and ${likes.length -2} others like this`;
    }
}

console.log(likes([])); // "no one likes this"
console.log(likes(["John"])); // "John likes this"
console.log(likes(["Mary", "Alex"])); // "Mary and Alex like this"
console.log(likes(["John", "James", "Linda"])); // "John, James and Linda like this"
console.log(likes(["Alex", "Linda", "Mark", "Max"])); // must be "Alex, Linda and 2 others 
console.log(likes(["Alex", "Linda", "Mark", "Max", "Jack", "Geoff"]));