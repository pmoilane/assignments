"use strict";
function getVowelCount(letters) {
    const vowels = ["a", "e", "i", "o", "u", "y"];
    const result = letters.split("").filter((letter) => vowels.includes(letter)).length;
    return result;
}
console.log(getVowelCount("abracadabra"));
