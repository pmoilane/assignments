import fs from "fs";

let example = fs.readFileSync("./src/example.txt", "utf-8");

console.log(example);

const exampleArray = example.split(" ");
for (let index = 0; index < exampleArray.length; index++) {
    if (exampleArray[index] === "joulu"){
        exampleArray[index] = "kinkku";
    } else if (exampleArray[index] === "lapsilla") {
        exampleArray[index] = "poroilla";
    }
}
example = exampleArray.join(" ");

fs.writeFileSync("modified.txt", example);
console.log(example);