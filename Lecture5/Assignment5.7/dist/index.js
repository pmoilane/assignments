"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
let example = fs_1.default.readFileSync("./src/example.txt", "utf-8");
console.log(example);
const exampleArray = example.split(" ");
for (let index = 0; index < exampleArray.length; index++) {
    if (exampleArray[index] === "joulu") {
        exampleArray[index] = "kinkku";
    }
    else if (exampleArray[index] === "lapsilla") {
        exampleArray[index] = "poroilla";
    }
}
example = exampleArray.join(" ");
fs_1.default.writeFileSync("modified.txt", example);
console.log(example);
