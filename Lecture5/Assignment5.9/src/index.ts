function calculator(operator: string, num1: number, num2: number) {
    if (["+", "-", "*", "/"].includes(operator)) {
        if (operator === "+") {
            return num1 + num2;
        } else if (operator === "-") {
            return num1 - num2;
        } else if (operator === "*") {
            return num1 * num2;
        } else if (operator === "/") {
            return num1 / num2;
        }
    } else {
        return `Can't do that, "${operator}" is an invalid operator, use "+", "-", "*" or "/" instead.`;
    }
}

console.log(calculator("+", 2, 3));
console.log(calculator("-", 2, 3));
console.log(calculator("*", 2, 3));
console.log(calculator("/", 2, 3));
console.log(calculator("#", 2, 3));

