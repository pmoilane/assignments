class Ingredient{
    name: string;
    amount: number;

    constructor(name: string, amount: number) {
        this.name = name;
        this.amount = amount;
    }
    scaleAmount(scaleFactor: number) {
        this.amount = this.amount * scaleFactor;
    }
}

class Recipe{
    name: string;
    ingredients: Array<Ingredient>;
    servings: number;

    constructor(name: string, ingredients: Array<Ingredient>, servings: number) {
        this.name = name;
        this.ingredients = ingredients;
        this.servings = servings;
    }
    toString()  {
        return `${this.name}, ainesosat: ${this.ingredients.map((ingredient) => ingredient.name + " " + ingredient.amount)}, ${String(this.servings)} annosta`;
    }
    setServings(newServings: number) {
        const scale = newServings / this.servings;
        this.ingredients.forEach((ingredient) => ingredient.scaleAmount(scale));
        this.servings = newServings;
    }
}

const maito = new Ingredient("maito", 2);
const jauhot = new Ingredient("jauhot", 3);
const voi = new Ingredient("voi", 5);
const sekoitus = new Recipe("sekoitus", [maito, jauhot, voi], 5);

console.log(sekoitus.toString());
jauhot.scaleAmount(3);
console.log(sekoitus.toString());
sekoitus.setServings(10);
console.log(sekoitus.toString());
