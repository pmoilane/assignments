const aString = "test";
let aNumber = 7;
const aBoolean = true;

console.log(aString, typeof(aString));
console.log(aNumber, typeof(aNumber));
console.log(aBoolean, typeof(aBoolean));

// let's change a number
aNumber = 5;

console.log(aString, typeof(aString));
console.log(aNumber, typeof(aNumber));
console.log(aBoolean, typeof(aBoolean));