const days = 365
const hours = 24
const seconds = 3600
const seconds_per_year = days * hours * seconds

console.log(`There are ${seconds_per_year} seconds in a year.`);

