// a
const playerCount = 4;

if (playerCount === 4){
    console.log("the game can be played");
} else {
    console.log("the game can't be played");
}

// b
const isStressed = true;
const hasIcecream = true;

if (!isStressed || hasIcecream) {
    console.log("Mark is happy");
} else {
    console.log("Mark is not happy");
}

// c
const isShining = true;
const isRaining = false;
const temperature = 22;

if (isShining && !isRaining && (temperature >= 20)) {
    console.log("It is a beach day");
} else {
    console.log("It's not a beach day"); 
}

// d
const seesSuzy = true;
const seesDan = true;
const weekday = "tuesday";

if ((seesSuzy || seesDan) && !(seesSuzy && seesDan) && (weekday === "tuesday")) {
    console.log("Arin is happy");
} else {
    console.log("Arin is sad");
}