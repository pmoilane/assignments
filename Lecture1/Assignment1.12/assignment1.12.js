const peopleAmount = process.argv[2];
const groupSize = process.argv[3];
const groupAmount = peopleAmount / groupSize

if (groupAmount < 1) {
    console.log("Not enough people for a group");
} else {
    console.log(`Number of groups: ${Math.ceil(groupAmount)}`);
}