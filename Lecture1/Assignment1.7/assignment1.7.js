const price = 20;
const discount = 50;
const discounted_price = price * ((100 - discount) * 0.01)

console.log(`Original price: ${price}€`);
console.log(`Discount: ${discount}%`);
console.log(`Discounted price ${discounted_price}€`);