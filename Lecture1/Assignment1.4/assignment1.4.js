// 1
const str1 = "yksi"
const str2 = "kaksi"
const str_sum = str1 + str2
console.log(`combined string: ${str_sum}`);
// 2
console.log(`length of str1: ${str1.length}`);
console.log(`length of str2: ${str2.length}`);
console.log(`length of str_sum: ${str_sum.length}`);
// 3
const avgLength = (str1.length + str2.length) / 2
console.log(`average length of str1 and str2: ${avgLength}`);
// 4
console.log(`str1 in uppercase: ${str1.toUpperCase()}`);
// 5
console.log(`first and last letter of str2: ${str2[0]} ${str2[str2.length - 1]}`);