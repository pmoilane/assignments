const lastName = "House";
const age = 51;
const isDoctor = true;
const sender = "Petteri";
const prefix = isDoctor ? "Dr." : "Mx.";
let nextAge = age + 1;

if (nextAge % 10 === 1) {
    nextAge = nextAge + "st"
} else if (nextAge % 10 === 2) {
    nextAge = nextAge + "nd"
} else if (nextAge % 10 === 3) {
    nextAge = nextAge + "rd"
} else {
    nextAge = nextAge + "th"
}

console.log(`Dear ${prefix} ${lastName}\n
Congratulations on your ${nextAge} birthday! Many happy returns!\n
Sincerely,
${sender}`);
