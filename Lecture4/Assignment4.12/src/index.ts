const arr = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22];

const divBy3 = arr.filter((num) => num % 3 === 0);
console.log(divBy3);

const mulBy2 = arr.map((num) => num * 2);
console.log(mulBy2);

const sumOfAll = arr.reduce((acc, cur) => acc + cur, 0);
console.log(sumOfAll);