const makeDice = (sides: number) => {
    return (throwAmount: number) => {
        return throwAmount * Math.floor(Math.random() * sides) + 1;
    };
};

const dice6 = makeDice(6);
const dice8 = makeDice(8);

const damage = dice6(2) + dice8(2);
console.log("You dealt " + damage + " damage");