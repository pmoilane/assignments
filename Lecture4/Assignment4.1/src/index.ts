const testString = "all letters are lowercase";

function UpperFirstLetters(a: string) {
    const words = a.split(" ");
    const result = [];
    for (const key in words){
        const word = words[key];
        result.push(word[0].toUpperCase() + word.slice(1));
    }
    return result.join(" ");
}
console.log(UpperFirstLetters(testString));