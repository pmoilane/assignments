
const joinStrings = (strArr: string[], separator: string) => strArr.reduce((acc, cur, index, arr) => (index < arr.length -1) ? acc + cur + separator : acc + cur, "");

const stringList = ["test1", "test2", "test3"];

console.log(joinStrings(stringList, ", "));