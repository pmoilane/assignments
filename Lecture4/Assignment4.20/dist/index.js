"use strict";
const n = Number(process.argv[2]);
const fibonacci = [0, 1];
for (let index = 1; fibonacci.length <= n; index++) {
    fibonacci.push(fibonacci[index - 1] + fibonacci[index]);
}
console.log(fibonacci);
