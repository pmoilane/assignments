function sumOfNumsToBillion(callback: (a: number) => void) {
    let sum = 0;
    for (let index = 0; index < 1e9; index++) {
        if (index % 3 === 0 && index % 5 === 0 && index % 7 === 0) {
            sum += index;
        }
    }
    callback(sum);
}

function Logger(input: number) {
    console.log(input);
}

sumOfNumsToBillion(Logger);
