const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];
const randomArray = [];

function randomNumberGenerator(max: number): number {
    return Math.floor(Math.random() * max);
}

while (array.length > 0) {
    randomArray.push(array.splice(randomNumberGenerator(array.length),1));
}

console.log(randomArray);