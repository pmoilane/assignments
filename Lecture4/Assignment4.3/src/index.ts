function sum1(a: number, b: number, c = 0) {
    return a + b + c;
}
console.log(sum1(1, 2, 3));

const sum2 = function(a: number, b: number, c = 0) {
    return a + b + c;
};
console.log(sum2(1, 2));

const sum3 = (a: number, b: number, c = 0) => a + b + c;
console.log(sum3(3, 2, 5));