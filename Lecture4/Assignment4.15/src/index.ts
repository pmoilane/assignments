const competitors = ["Julia", "Mark", "Spencer", "Ann" , "John", "Joe"];
const ordinals = ["st", "nd", "rd", "th"];

const placements = competitors.map((competitor, index) => {
    if (index < 3){
        return (index + 1) + ordinals[index] + " competitor was " + competitor;
    } else {
        return (index + 1) + ordinals[3] + " competitor was " + competitor;
    }
}
);

console.log(placements);