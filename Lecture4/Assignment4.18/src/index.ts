const inputStr = process.argv[2];
let result = "Yes, '" + inputStr + "', is a palindrome";

for (let index = 0; index < inputStr.length / 2; index++) {
    if (inputStr[index] !== inputStr[inputStr.length - (index + 1)]) {
        result = "No, '" + inputStr + "', is not a palindrome";
        break;
    }
}

console.log(result);