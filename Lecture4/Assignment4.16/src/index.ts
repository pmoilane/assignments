const start = Number(process.argv[2]);
const end = Number(process.argv[3]);
const arr = [];
let direction = 1;

if (start > end) {
    direction = -1;
}

for (let index = start; index != end + direction; index += direction) {
    arr.push(index);
}

console.log(arr);