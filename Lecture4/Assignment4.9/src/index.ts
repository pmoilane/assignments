const numbers = [
    749385,
    498654,
    234534,
    345467,
    956876,
    365457,
    235667,
    464534,
    346436,
    873453
];

const filterFunction = (numberList: number[]) => numberList.filter((aNumber) => ((aNumber % 3 === 0 || aNumber % 5 === 0) && !(aNumber % 3 === 0 && aNumber % 5 === 0)));
console.log(filterFunction(numbers));