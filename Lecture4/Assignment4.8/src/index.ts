const words = [
    "city",
    "distribute",
    "battlefield",
    "relationship",
    "spread",
    "orchestra",
    "directory",
    "copy",
    "raise",
    "ice"
];

const reversedWords = (wordList: string[]) => wordList.map((word) => word.split("").reverse().join(""));
console.log(reversedWords(words));