"use strict";
const inputStr = process.argv[2];
const wordList = inputStr.split(" ");
const reversedWords = wordList.map((word) => word.split("").reverse().join("")).join(" ");
console.log(reversedWords);
