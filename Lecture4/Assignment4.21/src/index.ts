const num = Number(process.argv[2]);

for (let index = 2; index <= num; index++) {
    if (num % index === 0 && num != index) {
        console.log(num + " is not a prime number");
        break;
    } else if (num === index){
        console.log(num + " is a prime number");
    }
}