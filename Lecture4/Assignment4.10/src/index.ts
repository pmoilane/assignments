const names = [
    "Murphy",
    "Hayden",
    "Parker",
    "Arden",
    "George",
    "Andie",
    "Ray",
    "Storm",
    "Tyler",
    "Pat",
    "Keegan",
    "Carroll"
];

const findName = (nameList: string[]) => nameList.find((name) => (name.length === 3 && name[2] === "t"));
console.log(findName(names));