1. 
SELECT * FROM public.loans WHERE loan_date < '2000-03-01';
2. 
SELECT * FROM public.loans WHERE return_date IS NOT null;
3. 
SELECT full_name, loans.loan_date 
FROM users 
JOIN loans 
ON users.id = loans.user_id
WHERE users.id = 1;
4. 
SELECT books.name, books.release_year, languages.name
FROM books
JOIN languages
ON books.language_id = languages.id
WHERE books.release_year > 1960;