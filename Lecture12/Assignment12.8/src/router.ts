import express from "express"
import { addProduct, deleteProduct, listAllProducts, readProduct, updateProduct } from "./dao"
import { Request, Response } from "express"

const router = express.Router()

router.post("/", async (req: Request, res: Response) => {
	const { name, price } = req.body
	const id = await addProduct(name, price)
	res.send({ id, name, price })
})

router.get("/", async (req: Request, res: Response) => {
	const result = await listAllProducts()
	res.send(result)
})

router.get("/:id", async (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const product = await readProduct(id)
	res.send(product)
})

router.put("/:id", async (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const { name, price} = req.body
	const product = await updateProduct(id, name, price)
	res.send(product)
})

router.delete("/:id", async (req: Request, res: Response) => {
	const id = Number(req.params.id)
	await deleteProduct(id)
	res.status(200).send(`Deleted product with id: ${id}`)
})

export default router