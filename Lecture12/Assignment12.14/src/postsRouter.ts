import express from "express"
import { readAllPosts, readPost, addPost } from "./dao"
import { Request, Response } from "express"

const postsRouter = express.Router()

postsRouter.get("/", async (req: Request, res: Response) => {
	const result = await readAllPosts()
	res.send(result)
})

postsRouter.get("/:id", async (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const post = await readPost(id)
	res.send(post)
})

postsRouter.post("/", async (req: Request, res: Response) => {
	const { userId, title, content } = req.body
	const id = await addPost(userId, title, content)
	res.send({ id, userId, title, content })
})

export default postsRouter