import express from "express"
import { addComment, readComments } from "./dao"
import { Request, Response } from "express"

const commentsRouter = express.Router()

commentsRouter.get("/:id", async (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const post = await readComments(id)
	res.send(post)
})

commentsRouter.post("/", async (req: Request, res: Response) => {
	const { userId, postId, content } = req.body
	const id = await addComment(userId, postId, content)
	res.send({ id, userId, postId, content })
})

export default commentsRouter