INSERT INTO users (username, full_name, email) 
VALUES 
    ('Tepe', 'Teppo Testaaja', 'teppo.testaaja@buutti.com'),
    ('Taitsa', 'Taija Testaaja', 'taija.testaaja@buutti.com'),
    ('Outi','Outi Ohjelmoija', 'outi.ohjelmoija@buutti.com'),
    ('Olli', 'Olli Ohjelmoija', 'olli.ohjelmoija@buutti.com'),
    ('Masa', 'Matti Meikäläinen', 'matti.meikalainen@buutti.com');

INSERT INTO posts (user_id, title, content, date)
VALUES
    (1, 'Testi123', 'Tämä on testi', NOW()),
    (2, 'Ilmoitus', 'Tärkeä ilmoitus :D', NOW()),
    (4, 'Testi','Tämäkin on testi', NOW()),
    (5, 'Myydään tietokone', 'hyvin pyörii pelit', NOW()),
    (5, 'Makkara resepti', 'Grillaa makkara, laita sinappia.', NOW());

INSERT INTO comments (user_id, post_id, content, date)
VALUES
    (4, 1, 'hyvin testattu', NOW()),
    (5, 2, ':D', NOW()),
    (1, 3, 'ok', NOW()),
    (2, 4, 'tarjoan 100€', NOW()),
    (2, 5, 'Hyvältä maistuu', NOW());