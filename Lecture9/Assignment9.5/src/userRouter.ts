import express, {Request, Response} from "express"
import argon2 from "argon2"
import "dotenv/config"

const userRouter = express.Router()
const users: Array<{username: string, password: string}> = []

userRouter.post("/register", async (req: Request, res: Response) => {
	const userInfo = req.body
	if (typeof userInfo.username !== "string" || typeof userInfo.password !== "string") {
		res.status(400).send("ERROR, missing parameter or wrong type")
	} else {
		userInfo.password = await argon2.hash(userInfo.password)
		users.push(userInfo)
		console.log(userInfo)
		res.status(201).send()
	}
})

userRouter.post("/login", async (req: Request, res: Response) => {
	const userInfo = req.body
	if (typeof userInfo.username !== "string" || typeof userInfo.password !== "string") {
		res.status(400).send("ERROR, missing parameter or wrong type")
	} else {
		const storedUserInfo = users.find(item => item.username === userInfo.username)
		if (storedUserInfo === undefined) {
			res.status(401).send("username not found")
		} else {
			if (await argon2.verify(storedUserInfo.password,userInfo.password)) {
				res.status(204).send()
			} else {
				res.status(401).send("Wrong password")
			}
		}
	}
})

userRouter.post("/admin", async (req: Request, res: Response) => {
	const { ADMINUSERNAME, ADMINPASSWORD } = process.env
	if (await argon2.verify(ADMINPASSWORD,req.body.password) && ADMINUSERNAME === req.body.username) {
		res.status(204).send()
	} else {
		res.status(401).send("Wrong password")
	}
})

export default userRouter
