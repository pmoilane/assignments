import express, {Request, Response} from "express"

const studentRouter = express.Router()

const students: Array<{id: number,name: string, email: string}> = []

studentRouter.get("/", (req: Request, res: Response) => {
	res.send(students.map((student) => student.id))
})

studentRouter.post("/", (req: Request, res: Response) => {
	const student = req.body
	if (typeof student.id !== "number" || typeof student.name !== "string" || typeof student.email !== "string") {
		res.status(400).send("ERROR, missing parameter or wrong type")
	} else {
		const studentInfo = {id: student.id, name: student.name, email: student.email}
		students.push(studentInfo)
		res.status(201).send()
	}
})

studentRouter.get("/:id", (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const info = students.find(item => item.id === id)
	if (info === undefined) {
		res.status(404).send("ERROR, student id not found")
	} else {
		res.send(info)
	}
})

studentRouter.put("/:id", (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const student = req.body
	const index = students.findIndex(item => item.id === id)
	if (index === -1) {
		res.status(404).send("ERROR, student id not found")
	} else {
		if (typeof student.name !== "string" && typeof student.email !== "string") {
			res.status(400).send("ERROR, incorrect parameters or parameter type")
			
		} else {
			if (typeof student.name === "string") {
				students[index].name = student.name
			}
			if (typeof student.email === "string") {
				students[index].email = student.email
			}
			res.status(204).send()
		}
	}
})

studentRouter.delete("/:id", (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const index = students.findIndex(item => item.id === id)
	if (index === -1) {
		res.status(404).send("ERROR, student id not found")
	} else {
		students.splice(index, 1)
		res.status(204).send()
	}
})

export default studentRouter