import request from "supertest"
import server from "../src/server"

describe("Server", () => {
	it("Returns 404 on invalid request", async () => {
		const response = await request(server)
			.get("/user/login")
		expect(response.statusCode).toBe(404)
	})
	it("Returns 200 when registering", async () => {
		const response = await request(server)
			.post("/user/register")
			.send({ username: "käyttäjä", password: "salasana" })
		expect(response.statusCode).toBe(200)
	})
	it("Returns 400 when registering with wrong parameter typer", async () => {
		const response = await request(server)
			.post("/user/register")
			.send({ username: "käyttäjä2", password: 12345 })
		expect(response.statusCode).toBe(400)
	})
	it("Returns 200 when logging in", async () => {
		const response = await request(server)
			.post("/user/login")
			.send({ username: "käyttäjä", password: "salasana" })
		expect(response.statusCode).toBe(200)
	})
	it("Returns 401 when logging in with wrong password", async () => {
		const response = await request(server)
			.post("/user/login")
			.send({ username: "käyttäjä", password: "sala" })
		expect(response.statusCode).toBe(401)
	})
	it("Returns 401 when logging in with unknown username", async () => {
		const response = await request(server)
			.post("/user/login")
			.send({ username: "käyttäjää", password: "salasana" })
		expect(response.statusCode).toBe(401)
	})
	it("Returns 400 when logging in with wrong parameter type", async () => {
		const response = await request(server)
			.post("/user/login")
			.send({ username: "käyttäjä", password: 213456 })
		expect(response.statusCode).toBe(400)
	})
})
