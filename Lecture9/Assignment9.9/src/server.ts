import express from "express"
import { logger, unknownEndpoint } from "./middlewares"
import studentRouter from "./studentRouter"
import userRouter from "./userRouter"

const server = express()
server.use(express.json())

server.use(express.static("public"))

server.use(logger)

server.use("/students", studentRouter)
server.use("/user", userRouter)

server.use(unknownEndpoint)

export default server