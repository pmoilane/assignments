import express, {NextFunction, Request, Response} from "express"
import jwt, { JwtPayload } from "jsonwebtoken"

const studentRouter = express.Router()

interface CustomRequest extends Request {
	user?: string | JwtPayload
}

const students: Array<{id: number,name: string, email: string}> = []

const authenticate = (req: CustomRequest, res: Response, next: NextFunction) => {
	const auth = req.get("Authorization")
	if (!auth?.startsWith("Bearer ")) {
		return res.status(401).send("Invalid token")
	}
	const token = auth.substring(7)
	const secret = process.env.SECRET
	try {
		const decodedToken = jwt.verify(token, secret)
		req.user = decodedToken
		next()
	} catch (error) {
		return res.status(401).send("Invalid token")
	}
}


studentRouter.get("/", authenticate, (req: Request, res: Response) => {
	res.send(students.map((student) => student.id))
})

studentRouter.post("/", authenticate, (req: Request, res: Response) => {
	const student = req.body
	if (typeof student.id !== "number" || typeof student.name !== "string" || typeof student.email !== "string") {
		res.status(400).send("ERROR, missing parameter or wrong type")
	} else {
		const studentInfo = {id: student.id, name: student.name, email: student.email}
		students.push(studentInfo)
		res.status(201).send()
	}
})

studentRouter.get("/:id", authenticate, (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const info = students.find(item => item.id === id)
	if (info === undefined) {
		res.status(404).send("ERROR, student id not found")
	} else {
		res.send(info)
	}
})

studentRouter.put("/:id", authenticate, (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const student = req.body
	const index = students.findIndex(item => item.id === id)
	if (index === -1) {
		res.status(404).send("ERROR, student id not found")
	} else {
		if (typeof student.name !== "string" && typeof student.email !== "string") {
			res.status(400).send("ERROR, incorrect parameters or parameter type")
			
		} else {
			if (typeof student.name === "string") {
				students[index].name = student.name
			}
			if (typeof student.email === "string") {
				students[index].email = student.email
			}
			res.status(204).send()
		}
	}
})

studentRouter.delete("/:id", authenticate, (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const index = students.findIndex(item => item.id === id)
	if (index === -1) {
		res.status(404).send("ERROR, student id not found")
	} else {
		students.splice(index, 1)
		res.status(204).send()
	}
})

export default studentRouter