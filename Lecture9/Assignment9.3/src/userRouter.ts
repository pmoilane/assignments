import express, {Request, Response} from "express"
import argon2 from "argon2"

const userRouter = express.Router()
const users: Array<{username: string, password: string}> = []

userRouter.post("/", async (req: Request, res: Response) => {
	const userInfo = req.body
	if (typeof userInfo.username !== "string" || typeof userInfo.password !== "string") {
		res.status(400).send("ERROR, missing parameter or wrong type")
	} else {
		userInfo.password = await argon2.hash(userInfo.password)
		users.push(userInfo)
		console.log(userInfo)
		res.status(201).send()
	}
})

export default userRouter