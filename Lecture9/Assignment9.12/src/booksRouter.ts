import express, {NextFunction, Request, Response} from "express"
import { checkInput, logReader } from "./middlewares"
import jwt from "jsonwebtoken"

const booksRouter = express.Router()

const books: Array<{id: number,name: string, author: string, read: boolean}> = []

interface jwtToken {
	username: string,
	isAdmin: boolean,
	iat: number,
	exp: number
}

interface CustomRequest extends Request {
	user?: jwtToken
}

const authenticate = (req: CustomRequest, res: Response, next: NextFunction) => {
	const auth = req.get("Authorization")
	if (!auth?.startsWith("Bearer ")) {
		return res.status(401).send("Invalid token")
	}
	const token = auth.substring(7)
	const secret = process.env.SECRET
	try {
		const decodedToken = jwt.verify(token, secret) as jwtToken
		req.user = decodedToken
		next()
	} catch (error) {
		return res.status(401).send("Invalid token")
	}
}

booksRouter.get("/", authenticate, (req: CustomRequest, res: Response) => {
	res.send(books)
})

booksRouter.get("/logs", authenticate, (req: CustomRequest, res: Response) => {
	logReader(req, res)
})

booksRouter.get("/:id", authenticate, (req: CustomRequest, res: Response, next: NextFunction) => {
	const id = Number(req.params.id)
	const info = books.find(item => item.id === id)
	if (info === undefined) {
		const error = new Error("Book id not found")
		next(error)
	} else {
		res.send(info)
	}
})

booksRouter.use(checkInput)

booksRouter.post("/", authenticate, (req: CustomRequest, res: Response) => {
	if (req.user.isAdmin) {
		const book = req.body
		books.push(book)
		res.status(201).send()
	} else {
		res.status(401).send("you are not authorized to use this")
	}
	
})

booksRouter.put("/:id", authenticate, (req: CustomRequest, res: Response, next: NextFunction) => {
	if (req.user.isAdmin) {
		const id = Number(req.params.id)
		const book = req.body
		const index = books.findIndex(item => item.id === id)
		if (index === -1) {
			const error = new Error("Book id not found")
			next(error)
		} else {
			if (typeof book.name === "string") {
				books[index].name = book.name
			}
			if (typeof book.author === "string") {
				books[index].author = book.author
			}
			if (typeof book.read === "boolean") {
				books[index].read = book.read
			}
			res.status(204).send()
		}
	} else {
		res.status(401).send("you are not authorized to use this")
	}
	
})

booksRouter.delete("/:id", authenticate, (req: CustomRequest, res: Response, next: NextFunction) => {
	if (req.user.isAdmin) {
		const id = Number(req.params.id)
		const index = books.findIndex(item => item.id === id)
		if (index === -1) {
			const error = new Error("Id not found")
			next(error)
		} else {
			books.splice(index, 1)
			res.status(204).send()
		}
	} else {
		res.status(401).send("you are not authorized to use this")
	}
	
})

export default booksRouter