import request from "supertest"
import server from "../src/server"

//let userToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImvDpHl0dMOkasOkIiwiaXNBZG1pbiI6ZmFsc2V9.FJKomaquckOnsBZHJBA1JHmFK6VijFiCnxcN-T4H5C4"
//let adminToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaXNBZG1pbiI6dHJ1ZX0.XPet1aA6WZ7sqTp_rcQOz_UvLeGbHoSoClHmtegkQiA"
let userToken: string
let adminToken: string

describe("Server", () => {
	it("Returns 404 on invalid endpoint", async () => {
		const response = await request(server)
			.get("/api/v1/users/endpoint")
		expect(response.statusCode).toBe(404)
	})
	it("Returns 200 when registering", async () => {
		const response = await request(server)
			.post("/api/v1/users/register")
			.send({ username: "käyttäjä", password: "salasana" })
		expect(response.statusCode).toBe(200)
	})
	it("Returns 400 when trying to register with username admin", async () => {
		const response = await request(server)
			.post("/api/v1/users/register")
			.send({ username: "admin", password: "salasana" })
		expect(response.statusCode).toBe(400)
	})
	it("Returns 200 when logging in", async () => {
		const response = await request(server)
			.post("/api/v1/users/login")
			.send({ username: "käyttäjä", password: "salasana" })
		expect(response.statusCode).toBe(200)
		userToken = response.text
	})
	it("Returns 200 when logging in as admin", async () => {
		const response = await request(server)
			.post("/api/v1/users/login")
			.send({ username: "admin", password: "salasana1" })
		expect(response.statusCode).toBe(200)
		adminToken = response.text
	})
	it("Returns 401 when trying to post as regular user", async () => {
		const response = await request(server)
			.post("/api/v1/books")
			.set("Authorization", `Bearer ${userToken}`)
			.send({ id: 1, name: "Kirja 1", author: "Kirjailija 1", read: false })
		expect(response.statusCode).toBe(401)
	})
	it("Returns 201 when posting as admin", async () => {
		const response = await request(server)
			.post("/api/v1/books")
			.set("Authorization", `Bearer ${adminToken}`)
			.send({ id: 1, name: "Kirja 1", author: "Kirjailija 1", read: false })
		expect(response.statusCode).toBe(201)
	})
	it("Returns 400 when trying to use put as regular user", async () => {
		const response = await request(server)
			.post("/api/v1/books/1")
			.set("Authorization", `Bearer ${userToken}`)
			.send({ read: true })
		expect(response.statusCode).toBe(400)
	})
	it("Returns 204 when using put as admin", async () => {
		const response = await request(server)
			.put("/api/v1/books/1")
			.set("Authorization", `Bearer ${adminToken}`)
			.send({ read: true })
		expect(response.statusCode).toBe(204)
	})
	it("Returns 200 when using get /books as regular user", async () => {
		const response = await request(server)
			.get("/api/v1/books")
			.set("Authorization", `Bearer ${userToken}`)
		expect(response.statusCode).toBe(200)
	})
	it("Returns 200 when using get /books/:id as regular user", async () => {
		const response = await request(server)
			.get("/api/v1/books/1")
			.set("Authorization", `Bearer ${userToken}`)
		expect(response.statusCode).toBe(200)
	})
	it("Returns 401 when trying to use delete as regular user", async () => {
		const response = await request(server)
			.delete("/api/v1/books/1")
			.set("Authorization", `Bearer ${userToken}`)
		expect(response.statusCode).toBe(401)
	})
	it("Returns 204 when using delete as admin", async () => {
		const response = await request(server)
			.delete("/api/v1/books/1")
			.set("Authorization", `Bearer ${adminToken}`)
		expect(response.statusCode).toBe(204)
	})
})