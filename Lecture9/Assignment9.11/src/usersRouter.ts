import express, {Request, Response} from "express"
import argon2 from "argon2"
import "dotenv/config"
import jwt from "jsonwebtoken"

const userRouter = express.Router()
const users: Array<{username: string, password: string}> = []
users.push({ username: "admin", password: process.env.ADMINPASSWORD })

userRouter.post("/register", async (req: Request, res: Response) => {
	const userInfo = req.body
	if (typeof userInfo.username !== "string" || typeof userInfo.password !== "string") {
		res.status(400).send("ERROR, missing parameter or wrong type")
	} else {
		const storedUserInfo = users.find(item => item.username === userInfo.username)
		if (storedUserInfo === undefined) {
			userInfo.password = await argon2.hash(userInfo.password)
			users.push(userInfo)
			res.status(200).send({ jwt: generateJwt(userInfo.username) })
		} else {
			res.status(400).send("username already exists")
		}
	}  
})

userRouter.post("/login", async (req: Request, res: Response) => {
	const userInfo = req.body
	if (typeof userInfo.username !== "string" || typeof userInfo.password !== "string") {
		res.status(400).send("ERROR, missing parameter or wrong type")
	} else {
		const storedUserInfo = users.find(item => item.username === userInfo.username)
		if (storedUserInfo === undefined) {
			res.status(401).send("username not found")
		} else {
			if (await argon2.verify(storedUserInfo.password,userInfo.password)) {
				res.status(200).send({ jwt: generateJwt(userInfo.username) })
			} else {
				res.status(401).send("Wrong password")
			}
		}
	}
})

const generateJwt = (username: string): string => {
	const isAdmin = username === "admin" ? true : false
	const secret = process.env.SECRET
	const payload = { username: username, isAdmin: isAdmin }
	const options = { expiresIn: "15min"}
	return jwt.sign(payload, secret, options)
}

export default userRouter