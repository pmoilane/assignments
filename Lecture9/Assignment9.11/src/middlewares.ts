import { Request, Response, NextFunction} from "express"

const logs = []

export const checkInput = (req: Request, res: Response, next: NextFunction) => {
	const method = req.method
	const book = req.body
	if (method === "POST") {
		if (typeof book.id !== "number" || typeof book.name !== "string" || typeof book.author !== "string" || typeof book.read !== "boolean") {
			res.status(400).send("ERROR, missing parameter or wrong type")
		} else {
			next()
		}
	} else if (method === "PUT") {
		if (typeof book.name !== "string" && typeof book.author !== "string" && typeof book.read !== "boolean") {
			res.status(400).send("ERROR, incorrect parameters or parameter type")
		} else {
			next()
		}
	} else {
		next()
	}
}

export const unknownEndpoint = (_req: Request, _res: Response ,next: NextFunction) => {
	const error = new Error("Endpoint not valid")
	next(error)
}

export const logger = (req: Request, res: Response, next: NextFunction) => {
	const now = new Date()
	const method = req.method
	const url = req.url
	const body = req.body
	const logData = `${now.toLocaleString()} ${method} ${url}`
	if (!body) {
		console.log(logData)
		logs.push(logData)
	} else {
		console.log(`${logData} ${JSON.stringify(body)}`)
		logs.push(`${logData} ${JSON.stringify(body)}`)
	}
	next()
}

export const logReader = (req: Request, res: Response) => {
	res.status(200).send(logs)
}