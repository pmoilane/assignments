import express, { NextFunction, Request, Response} from "express"
import { unknownEndpoint, logger } from "./middlewares"
import helmet from "helmet"
import booksRouter from "./booksRouter"
import userRouter from "./usersRouter"

const server = express()
server.use(helmet())
server.use(express.json())
server.use(logger)

server.use("/api/v1/books/", booksRouter)
server.use("/api/v1/users/", userRouter)

const errorHandler = (error: Error, req: Request, res: Response, next: NextFunction) => {
	res.status(404).send(error.message)
	next(error)
}

server.use(unknownEndpoint)
server.use(errorHandler)
server.listen(3000)
