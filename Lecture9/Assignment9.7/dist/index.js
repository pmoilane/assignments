"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv/config");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const payload = { username: "käyttäjä" };
const secret = process.env.SECRET;
const options = { expiresIn: "15min" };
const testToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoia8OkeXR0w6Rqw6QifQ.j8hYpGz3-HT9_25-0-5mkQkVnsu2RUgdy20ZGIcEMiw";
const token = (_a = process.argv[2]) !== null && _a !== void 0 ? _a : testToken;
try {
    const result = jsonwebtoken_1.default.verify(token, secret);
    console.log(result);
}
catch (error) {
    console.log("wrong token");
}
