import "dotenv/config"
import jwt from "jsonwebtoken"

const secret = process.env.SECRET

const testToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoia8OkeXR0w6Rqw6QifQ.j8hYpGz3-HT9_25-0-5mkQkVnsu2RUgdy20ZGIcEMiw"
const token = process.argv[2] ?? testToken

try {
	const result = jwt.verify(token, secret)
	console.log(result)
} catch (error) {
	console.log("wrong token")
}
