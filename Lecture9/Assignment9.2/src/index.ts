import express from "express"
import { logger, unknownEndpoint } from "./middlewares"
import studentRouter from "./studentRouter"

const server = express()
server.use(express.json())

server.use(express.static("public"))

server.use(logger)

server.use("/students", studentRouter)

server.use(unknownEndpoint)

server.listen(3000)
