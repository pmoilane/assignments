interface Props {
    nameList: Array<string>
}

const ListNames = (props: Props) => {

    return(
        <div>{props.nameList.map(
            (person, i) => {
                if (i % 2 === 0) {
                    return <div key={"names" + i}><b>{person}</b></div>
                } else {
                    return <div key={"names" + i}><i>{person}</i></div>
                }   
            })}
        </div>
    )
}

export default ListNames