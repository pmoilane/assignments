//import './App.css'
import ListNames from './ListNames'

function App() {
  const namelist = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"]

  return (
    <>
      <ListNames nameList={namelist}/>
    </>
  )
}

export default App
