import { useState, ChangeEvent } from "react"

export interface TodoItem {
  name: string
  isDone: boolean
}

function Todo() {
  const [inputString, setInputString] = useState("")
  const [listOfItems, setListOfItems] = useState<TodoItem[]>([])

  const onInputStringChange = (event: ChangeEvent<HTMLInputElement>) => {
    setInputString(event.target.value)
  }

  const onClickSubmit = () => {
    setListOfItems([
      ...listOfItems,
      { name: inputString, isDone: false}
    ])}

  const removeItem = (index: number) => {
    setListOfItems(listOfItems.filter((_item, i) => i != index))
  }

  const toggleIsDone = (index: number) => {
    setListOfItems(listOfItems.map((item, i) => {
      const newItem = i === index 
        ? { ...item, isDone: !item.isDone }
        : item
      return newItem
    }))
  }

  return (
    <div>
      <h1>Todo list:</h1>
      <div>Add an item:</div>
      <input value={inputString}
      onChange={onInputStringChange} />
      <button onClick={onClickSubmit}>Submit</button>
      <div>{listOfItems.map(
        (item, i) => {return(
        <div key={"items" + i}>
          {item.name} Done:
          <input type="checkbox" 
          checked={item.isDone}
          onChange={() => toggleIsDone(i)}></input>
          <button onClick={() => removeItem(i)}>
            Delete
          </button>
        </div>
        )}
        )}
        </div>
    </div>
  )
}

export default Todo