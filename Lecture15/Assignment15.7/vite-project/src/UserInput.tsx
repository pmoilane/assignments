import { useState, ChangeEvent } from "react"

function UserInput() {
  const [inputString, setInputString] = useState("")
  const [myString, setMyString] = useState("")

  const onInputStringChange = (event: ChangeEvent<HTMLInputElement>) => {
    setInputString(event.target.value)
  }

  const onClickSubmit = () => {
    setMyString(inputString)
  }

  return (
    <div>
      <h1>Your string is: {myString}</h1>
      <input value={inputString}
      onChange={onInputStringChange} />
      <button onClick={onClickSubmit}>Submit</button>
    </div>
  )
}

export default UserInput