function RandomNumber() {
    const RandomNumber = Math.floor(Math.random() * 100 + 1)

    return (
        <div className="RandomNumber">
            Generated random number is: {RandomNumber}
        </div>
    )
  }
  
export default RandomNumber