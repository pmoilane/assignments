import RandomNumber from "./RandomNumber"

function App() {

  return (
    <>
      <h1>Random Number Generator</h1>
      <RandomNumber />
    </>
  )
}

export default App
