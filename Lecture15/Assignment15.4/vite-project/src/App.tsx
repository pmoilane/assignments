import Instrument from "./Instrument"
import guitarImg from "./assets/guitar.png"
import violinImg from "./assets/violin.png"
import tubaImg from "./assets/tuba.png"
import "./App.css"

function App() {
  return (
    <>
      <h1>Instruments</h1>
      <Instrument name="guitar"
      price={500}
      img={guitarImg}/>
      <Instrument name="violin"
      price={800}
      img={violinImg}/>
      <Instrument name="tuba"
      price={1700}
      img={tubaImg}/>
    </>
  )
}

export default App
