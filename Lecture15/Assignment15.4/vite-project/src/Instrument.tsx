
interface Props {
    name: string
    price: number
    img: string
}

const Instrument = (props: Props) => {
    return (
    <div>
        <img src={props.img} className="InstrumentImg"></img>
        <p className="InstrumentText">Instrument name: {props.name}</p>
        <p className="InstrumentText">Price: {props.price}</p>
    </div>
    )
}

export default Instrument