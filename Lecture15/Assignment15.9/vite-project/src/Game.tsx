import { useState } from "react"

export interface GameSquare {
  symbol: string
  isChecked: boolean
}

function Game() {

  const startArray: Array<GameSquare> = []
  for (let i = 0; i < 9; i++) {
    startArray.push({symbol: "□", isChecked: false})
  }

  const [listOfSquares, setListOfSquares] = useState<GameSquare[]>(startArray)
  const [turnOfPlayer1, setTurnOfPlayer1] = useState<boolean>(true)
  const [turnsLeft, setTurnsLeft] = useState(9)

  const playTurn = (index: number) => {
    if (turnsLeft > 0) {
      setListOfSquares(listOfSquares.map((item, i) => {
      const newSymbol = turnOfPlayer1
        ? "X"
        : "O"
      if (i === index && !item.isChecked) {
        changeTurn()
        setTurnsLeft(turns => turns - 1)
        return { symbol: newSymbol, isChecked: !item.isChecked }
      } else {
        return item
      }
    }))}
  }

  const changeTurn = () => {
    setTurnOfPlayer1(!turnOfPlayer1)
  }

  const resetGame = () => {
    const array: Array<GameSquare> = []
    for (let i = 0; i < 9; i++) {
      array.push({symbol: "□", isChecked: false})
    }
    setListOfSquares(array)
    setTurnsLeft(9)
    if (!turnOfPlayer1) {
      changeTurn()
    }
  }

  return (
    <div>
      <h1>Tic Tac Toe</h1> <div>{turnOfPlayer1 ? "Player 1" : "Player 2"}</div>
      <button onClick={() => resetGame()}> Reset game </button>
      <div>
        <button onClick={() => playTurn(0)}>
          {listOfSquares[0].symbol}
        </button>
        <button onClick={() => playTurn(1)}>
          {listOfSquares[1].symbol}
        </button>
        <button onClick={() => playTurn(2)}>
          {listOfSquares[2].symbol}
        </button>
      </div>
      <div>
        <button onClick={() => playTurn(3)}>
          {listOfSquares[3].symbol}
        </button>
        <button onClick={() => playTurn(4)}>
          {listOfSquares[4].symbol}
        </button>
        <button onClick={() => playTurn(5)}>
          {listOfSquares[5].symbol}
        </button>
      </div>
      <div>
        <button onClick={() => playTurn(6)}>
          {listOfSquares[6].symbol}
        </button>
        <button onClick={() => playTurn(7)}>
          {listOfSquares[7].symbol}
        </button>
        <button onClick={() => playTurn(8)}>
          {listOfSquares[8].symbol}
        </button>
      </div>
      <div>{turnsLeft <= 0 ? "Game has ended" : " "}</div>
    </div>
  )
}

export default Game
