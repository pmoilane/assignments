import { useState } from 'react'
//import './App.css'

function App() {
  const [count, setCount] = useState(0)
  const [count2, setCount2] = useState(0)
  const [count3, setCount3] = useState(0)
  const [count4, setCount4] = useState(0)

  return (
    <>
      <div>
        <button onClick={() => 
          {setCount((count) => count + 1)
            setCount4((count4) => count4 + 1)
          }}>
          {count}
        </button>
      </div>
      <div>
      <button onClick={() => 
          {setCount2((count2) => count2 + 1)
            setCount4((count4) => count4 + 1)
          }}>
          {count2}
        </button>
      </div>
      <div>
      <button onClick={() => 
          {setCount3((count3) => count3 + 1)
            setCount4((count4) => count4 + 1)
          }}>
          {count3}
        </button>
      </div>
      <div>
        {count4}
      </div>
    </>
  )
}

export default App
