const targetLetter = process.argv[2];
const replacementLetter = process.argv[3];
const inputString = process.argv[4];

console.log(inputString.replace(RegExp(`${targetLetter}`, "g"), replacementLetter));
