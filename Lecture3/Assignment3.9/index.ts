const n = 17;
let ind = 1;
while (ind <= 100) {
    if (ind % 3 === 0 && ind % 5 === 0){
        console.log("FizzBuzz");    
    } else if (ind % 3 === 0) {
        console.log("Fizz");
    } else if (ind % 5 === 0) {
        console.log("Buzz");
    } else {
        console.log(ind);  
    }
    ind = ind + 1;
}