const style = process.argv[2];
const phrase = process.argv[3];

if (style === "upper") {
    console.log(phrase.toUpperCase());
} else if (style === "lower") {
    console.log(phrase.toLowerCase());
}