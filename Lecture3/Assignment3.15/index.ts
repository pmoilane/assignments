const name1 = process.argv[2];
const name2 = process.argv[3];
const name3 = process.argv[4];

const initials = [name1[0], name2[0], name3[0]];
const lengths = [name1, name2, name3];

console.log(initials.join("."));
console.log(lengths.sort().reverse().join(" "));