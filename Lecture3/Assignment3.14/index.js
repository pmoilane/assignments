var balance = -5;
var isActive = true;
var checkBalance = true;
if (checkBalance) {
    if (balance > 0 && isActive) {
        console.log(balance);
    }
    else if (!isActive) {
        console.log("Your account is not active!");
    }
    else if (balance == 0) {
        console.log("Your account is empty");
    }
    else {
        console.log("Your balance is negative");
    }
}
else {
    console.log("Have a nice day!");
}
