let sum = 0;
const n = 17;
let ind = 1;
while (ind <= n) {
    if (ind % 3 === 0 || ind % 5 === 0){
        sum = sum + ind;    
    } 
    ind = ind + 1;
}
console.log(sum);
