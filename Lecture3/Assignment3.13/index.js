var monthNum = Number(process.argv[2]);
var months31 = [1, 3, 5, 7, 8, 10, 12];
var months30 = [4, 6, 9, 11];
if (months31.includes(monthNum)) {
    console.log("31 days");
}
else if (months30.includes(monthNum)) {
    console.log("30 days");
}
else if (monthNum === 2) {
    console.log("28 or 29 days");
}
