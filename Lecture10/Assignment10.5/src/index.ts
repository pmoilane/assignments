import express, { Request, Response } from "express"

const server = express() // server is of type "Express"

server.get("/dockertest", (_req: Request, res: Response) => {
	res.send("Hello from Docker!")
})

server.listen(3000, () => {
	console.log("Listening to port 3000")
})