const waitFor1sec = () => new Promise<void>((resolve, reject) => {
    setTimeout(() => {resolve()}, 1000)
})

const fibonacci = async () => {
    console.log(0)
    let previousNum = 0
    let num = 1
    let nextNum: number
    while (true) {
        await waitFor1sec()
        console.log(num)
        nextNum = num + previousNum
        previousNum = num
        num = nextNum
    }
}

fibonacci()