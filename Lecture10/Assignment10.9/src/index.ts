import express from "express"
import { unknownEndpoint, logger } from "./middlewares"
import boardRouter from "./boardRouter"

const server = express()
server.use(express.json())
server.use(logger)

server.use("/board/", boardRouter)

server.use(unknownEndpoint)
server.listen(3000)