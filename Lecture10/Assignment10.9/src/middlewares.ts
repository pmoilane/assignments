import { Request, Response, NextFunction} from "express"

const logs = []

export const unknownEndpoint = (_req: Request, _res: Response ,next: NextFunction) => {
	const error = new Error("Endpoint not valid")
	next(error)
}

export const logger = (req: Request, res: Response, next: NextFunction) => {
	const now = new Date()
	const method = req.method
	const url = req.url
	const body = req.body
	const logData = `${now.toLocaleString()} ${method} ${url}`
	if (!body) {
		console.log(logData)
		logs.push(logData)
	} else {
		console.log(`${logData} ${JSON.stringify(body)}`)
		logs.push(`${logData} ${JSON.stringify(body)}`)
	}
	next()
}