node:latest 1GB
node:slim 251MB
node:alpine 180MB
alpine:latest 68.5MB

alpine:latest to save space, one of the node ones for ease of use, would have to 
first check what they have preinstalled though, and then choose a suitable one.
But for this task alpine:latest is preferred.