import { Request, Response, NextFunction} from "express"

const logs = []

export const unknownEndpoint = (_req: Request, res: Response) => {
	res.status(404).send("ERROR, Endpoint not valid")
}

export const logger = (req: Request, res: Response, next: NextFunction) => {
	const now = new Date()
	const method = req.method
	const url = req.url
	const body = req.body
	const logData = `${now.toLocaleString()} ${method} ${url}`
	if (!body) {
		console.log(logData)
		logs.push(logData)
	} else {
		console.log(`${logData} ${JSON.stringify(body)}`)
		logs.push(`${logData} ${JSON.stringify(body)}`)
	}
	next()
}