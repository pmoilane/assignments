import express from "express"
import { unknownEndpoint, logger } from "./middlewares"
import boardRouter from "./boardRouter"
import secretBoardRouter from "./secretBoardRouter"

const server = express()
server.use(express.json())
server.use(logger)

server.use("/board/", boardRouter)
server.use("/secretboard/", secretBoardRouter)

server.use(unknownEndpoint)
server.listen(3000)

// for testing: secretboard accessible with Basic Auth username: salainenkäyttäjä password: salasana1