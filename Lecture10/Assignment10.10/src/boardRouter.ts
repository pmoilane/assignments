import express, {Request, Response} from "express"

const boardRouter = express.Router()

const messages: Array<{message: string}> = []

boardRouter.get("/", (req: Request, res: Response) => {
	res.send(messages.join("<br>"))
})

boardRouter.post("/", (req: Request, res: Response) => {
	messages.push(req.body.message)
	res.status(201).send()
})

boardRouter.delete("/", (req: Request, res: Response) => {
	const index = messages.findIndex(item => item === req.body.message)
	if (index === -1) {
		res.status(401).send("message not found")
	} else {
		messages.splice(index, 1)
		res.status(204).send()
	}
})

export default boardRouter