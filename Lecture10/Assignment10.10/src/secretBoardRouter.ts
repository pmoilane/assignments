import express, {Request, Response, NextFunction} from "express"
import "dotenv/config"
import argon2 from "argon2"

const secretBoardRouter = express.Router()

const secretMessages: Array<{message: string}> = []

const authenticate = async (req: Request, res: Response, next: NextFunction) => {
	const auth = req.get("Authorization")
	if (!auth?.startsWith("Basic ")) {
		return res.status(401).send("Invalid header")
	}
	const decodedUserInfo = Buffer.from(auth.substring(6), "base64").toString().split(":")

	if (decodedUserInfo[0] !== process.env.secretUsername) {
		res.status(401).send("username not found")
	} else {
		if (await argon2.verify(process.env.secretPassword,decodedUserInfo[1])) {
			next()
		} else {
			res.status(401).send("Wrong password")
		}
	}
}

secretBoardRouter.get("/", authenticate, (req: Request, res: Response) => {
	res.send(secretMessages.join("<br>"))
})

secretBoardRouter.post("/", authenticate, (req: Request, res: Response) => {
	secretMessages.push(req.body.message)
	res.status(201).send()
})

secretBoardRouter.delete("/", authenticate, (req: Request, res: Response) => {
	const index = secretMessages.findIndex(item => item === req.body.message)
	if (index === -1) {
		res.status(401).send("message not found")
	} else {
		secretMessages.splice(index, 1)
		res.status(204).send()
	}
})

export default secretBoardRouter