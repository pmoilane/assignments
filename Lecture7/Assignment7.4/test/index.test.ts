import calculator from "../src/index";

describe("division", () => {
    it("Returns 3 with parameters '/', 6 and 2", () => {
        const result = calculator("/", 6, 2);
        expect(result).toBe(3);
    });
    it("Returns -2 with parameters '/', 6 and -3", () => {
        const result = calculator("/", 6, -3);
        expect(result).toBe(-2);
    });
    it("Returns Infinity with parameters '/', 6 and 0", () => {
        const result = calculator("/", 6, 0);
        expect(result).toBe(Infinity);
    });
    it("Returns 'Invalid operator, use +, - * or / instead.' with parameters '//', 6 and 3", () => {
        const result = calculator("//", 6, 3);
        expect(result).toBe("Invalid operator, use +, - * or / instead.");
    });
    it("Returns NaN with parameters '/', 'asd' and 5", () => {
        const result = calculator("/", "asd" as any, 5);
        expect(result).toBe(NaN);
    });
});
