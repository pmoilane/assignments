import calculator from "../src/index";

describe("division", () => {
    it("Returns 3 with parameters '/', 6 and 2", () => {
        expect(calculator("/", 6, 2)).toBe(3);
    });
    it("Returns 15 with parameters '*', 6 and 2", () => {
        expect(calculator("*", 6, 2.5)).toBe(15);
    });
    it("Returns 9 with parameters '/', 6 and -3", () => {
        expect(calculator("-", 6, -3)).toBe(9);
    });
    it("Returns 6 with parameters '+', 6 and 0", () => {
        expect(calculator("+", 6, 0)).toBe(6);
    });
    it("Returns 'Invalid operator, use +, - * or / instead.' with parameters '//', 6 and 3", () => {
        expect(calculator("//", 6, 3)).toBe("Invalid operator, use +, - * or / instead.");
    });
});
