export default function calculator(operator, num1, num2) {
    if (["+", "-", "*", "/"].includes(operator)) {
        if (operator === "+") {
            return num1 + num2;
        }
        else if (operator === "-") {
            return num1 - num2;
        }
        else if (operator === "*") {
            return num1 * num2;
        }
        else if (operator === "/") {
            return num1 / num2;
        }
    }
    else {
        return "Invalid operator, use +, - * or / instead.";
    }
}
console.log(calculator("/", "asd", 5));
