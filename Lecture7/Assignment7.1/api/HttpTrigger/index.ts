import { AzureFunction, Context, HttpRequest } from "@azure/functions"

const httpTrigger: AzureFunction = async function (context: Context, req: HttpRequest): Promise<void> {
    context.log('HTTP trigger function processed a request.');
    
    let status, body;

    const name = req.body.name;

    if (!name) {
        status = 400,
        body = "Missing parameter 'name'"
    } else {
        status = 200,
        body = `Hello, ${name}`.toUpperCase()
    }
    
    context.res = {
        status,
        body
    };
};

export default httpTrigger;