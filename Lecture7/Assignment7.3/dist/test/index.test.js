import calculator from "../src/index";
test(" ", () => {
    expect(calculator("+", 2, 3)).toBe(5);
});
test(" ", () => {
    expect(calculator("-", 2, 3)).toBe(-1);
});
test(" ", () => {
    expect(calculator("*", 4, -2)).toBe(-8);
});
test(" ", () => {
    expect(calculator("/", 6, 2)).toBe(3);
});
test(" ", () => {
    expect(calculator("#", 6, 2)).toBe("Invalid operator, use +, - * or / instead.");
});
