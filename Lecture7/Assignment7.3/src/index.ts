export default function calculator(operator: string, num1: number, num2: number) {
    if (["+", "-", "*", "/"].includes(operator)) {
        if (operator === "+") {
            return num1 + num2;
        } else if (operator === "-") {
            return num1 - num2;
        } else if (operator === "*") {
            return num1 * num2;
        } else if (operator === "/") {
            return num1 / num2;
        }
    } else {
        return "Invalid operator, use +, - * or / instead.";
    }
}
