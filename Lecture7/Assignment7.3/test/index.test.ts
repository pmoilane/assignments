import calculator from "../src/index";

test(" ", () => {
    expect(calculator("*", 4, 2)).toBe(8);
});
test(" ", () => {
    expect(calculator("*", 4, -2)).toBe(-8);
});
test(" ", () => {
    expect(calculator("*", 4, 0)).toBe(0);
});