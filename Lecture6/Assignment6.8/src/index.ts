import axios from "axios";

interface User {
    name: string
    username: string
    email: string
}

async function getData() {
    const query = "https://jsonplaceholder.typicode.com/todos/";
    const response = await axios.get(query);
    const data = response.data;
    const userQuery = "https://jsonplaceholder.typicode.com/users/";
    const userResponse = await axios.get(userQuery);
    const userData = userResponse.data.map(({name, username, email}: User): User => ({name, username, email}));
    for (let i = 0; i < data.length; i++) {
        data[i].user = userData[data[i].userId - 1];
    }
    console.log(data);
}

getData();
/*
async function getUserDataById(userId: number) {
    const query = `https://jsonplaceholder.typicode.com/users/${userId}`;
    const response = await axios.get(query);
    const data = response.data;
    return data;
}

async function getData() {
    const query = "https://jsonplaceholder.typicode.com/todos/";
    const response = await axios.get(query);
    const data = response.data;
    for (let i = 0; i < data.length; i++) {
        data[i].user = await getUserDataById(data[i].userId);
    }
    console.log(await data);;
}
*/
