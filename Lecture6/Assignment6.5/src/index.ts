import axios from "axios";

interface Response {
    Title: string
    Release: string
    Genre: string
}

async function getMovieData(title: string, year?: number): Promise<void> {
    const query = `http://www.omdbapi.com/?apikey=c3a0092f&t=${title}&y=${year}`;
    const response = await axios.get(query);
    const typedResult: Response = response.data;
    console.log(typedResult);
}

getMovieData("the",2005);