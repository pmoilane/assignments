
function raceLap(): Promise<void> {
    return new Promise((resolve, reject) => {
        const crash = Math.random();
        if (crash <= 0.03) {
            reject();
        } else {
            resolve();
        }
    });
}    

async function race(drivers: Array<string>, laps: number) {
    const driverList: Array<{name: string, totalTime: number, bestLap: number, hasCrashed: boolean}> = [];
    for (let i = 0; i < drivers.length; i++) {
        driverList[i] = {name: drivers[i], totalTime: 0, bestLap: Infinity, hasCrashed: false};
    }
    for (let i = 0; i < laps; i++) {
        for (let j = 0; j < driverList.length; j++) {
            const lap = await raceLap()
                .then(() => rNG(20,25))
                .catch(() => 0);
            if (lap === 0) {
                driverList[j].hasCrashed = true;
            } else if (driverList[j].hasCrashed === false) {
                driverList[j].totalTime += lap;
                if (lap < driverList[j].bestLap)
                    driverList[j].bestLap = lap;
            }
        }
        
    }
    let lowestTotalTime = Infinity;
    let winnerId = 0;
    for (let i = 0; i < driverList.length; i++) {
        if (driverList[i].hasCrashed === false && driverList[i].totalTime < lowestTotalTime) {
            lowestTotalTime = driverList[i].totalTime;
            winnerId = i;
        }
    }
    console.log(driverList[winnerId]);
}

function rNG(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

race(["Häkkinen", "Räikkönen", "Bottas", "Kovalainen"], 15);
