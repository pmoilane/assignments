
const getValue = function (): Promise<number> {
    return new Promise((resolve, _reject) => {
        setTimeout(() => {
            resolve(Math.random());
        }, Math.random() * 1500);
    });
};

async function values() {
    const val1 = await getValue();
    const val2 = await getValue();
    console.log(val1, val2);
}
values();

const nums: Array<number> = [];
const values2 = () => {
    getValue().then((num) => {
        nums[0] = num;
        return getValue();
    }).then((num2) => {
        nums[1] = num2;
        console.log(nums[0], nums[1]);
    });
};
values2();