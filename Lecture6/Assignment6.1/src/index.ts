const countdownFunction = function (time: number, func: () => void): void {
    setTimeout(() => {
        func();
    },time);
};


console.log("3");
countdownFunction(1000, () => {
    console.log("2");
    countdownFunction(1000, () => {
        console.log("1");
        countdownFunction(1000, () => {
            console.log("GO!");
        });
    });
});