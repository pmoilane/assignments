function largestNumber(numArray: Array<number>): number {
    let largest = -Infinity;
    for (let index = 0; index < numArray.length; index++) {
        if (numArray[index] > largest) {
            largest = numArray[index];
        }
    }
    return largest;
}

function secondLargestNumber(numArray: Array<number>): number {
    const largest = largestNumber(numArray);
    let secondLargest = -Infinity;
    for (let index = 0; index < numArray.length; index++) {
        if (numArray[index] > secondLargest && numArray[index] < largest) {
            secondLargest = numArray[index];
        }
    }
    return secondLargest;
}

console.log(largestNumber([5,8,15,2,-3,4,16,-1,11]));
console.log(secondLargestNumber([5,8,15,2,-3,4,16,-1,11]));