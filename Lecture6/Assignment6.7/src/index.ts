const students = [
    { name: "Markku", score: 99 },
    { name: "Karoliina", score: 58 },
    { name: "Susanna", score: 69 },
    { name: "Benjamin", score: 77 },
    { name: "Isak", score: 49 },
    { name: "Liisa", score: 89 },
];

function highestAndLowestScore(students: Array<{name: string, score: number}>): Array<string> {
    let highestScore = -Infinity;
    let highestStudent = "";
    let lowestScore = Infinity;
    let lowestStudent = "";
    for (let index = 0; index < students.length; index++) {
        if (students[index].score > highestScore) {
            highestScore = students[index].score;
            highestStudent = students[index].name;
        }
        else if (students[index].score < lowestScore) {
            lowestScore = students[index].score;
            lowestStudent = students[index].name;
        }
    }
    return [highestStudent, lowestStudent];
}

function averageScore(students: Array<{name: string, score: number}>) {
    return students.reduce((acc, cur) => acc + cur.score, 0) / students.length;
}

function aboveAverageStudents(students: Array<{name: string, score: number}>): Array<{name: string, score: number}> {
    const result: Array<{name: string, score: number}> = [];
    const average = averageScore(students);
    for (let index = 0; index < students.length; index++) {
        if (students[index].score > average) {
            result.push(students[index]);
        }
    }
    return result;
}

function assignGrades(students: Array<{name: string, score: number}>) {
    const studentGrades: Array<{name: string, score: number, grade?: number}> = students;
    for (let index = 0; index < students.length; index++) {
        if (students[index].score > 1 && students[index].score <= 39) {
            studentGrades[index].grade = 1;
        } else if (students[index].score > 40 && students[index].score <= 59) {
            studentGrades[index].grade = 2;
        } else if (students[index].score > 60 && students[index].score <= 79) {
            studentGrades[index].grade = 3;
        } else if (students[index].score > 80 && students[index].score <= 94) {
            studentGrades[index].grade = 4;
        } else if (students[index].score > 95 && students[index].score <= 100) {
            studentGrades[index].grade = 5;
        }
    }
    return studentGrades;
}

console.log(highestAndLowestScore(students));
console.log(averageScore(students));
console.log(aboveAverageStudents(students));
console.log(assignGrades(students));