import express from "express"
import { logger, unknownEndpoint } from "./middlewares"
import friendRouter from "./friendRouter"

const server = express()
server.use(express.json())

server.use(express.static("public"))

server.use(logger)

server.use("/", friendRouter)

server.use(unknownEndpoint)

server.listen(3000)
