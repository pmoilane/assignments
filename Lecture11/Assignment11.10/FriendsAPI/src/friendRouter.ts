import express, {Request, Response} from "express"
import axios from "axios"

const friendRouter = express.Router()

interface Friend {
	username: string,
	name: string,
	email: string
}

const friends: Array<Friend> = []

friendRouter.get("/friends", (req: Request, res: Response) => {
	res.json(friends)
})

friendRouter.post("/", (req: Request, res: Response) => {
	const friend = req.body
	if (typeof friend.username !== "string" || typeof friend.name !== "string" || typeof friend.email !== "string"){
		res.status(400).send("ERROR, missing parameter or wrong type")
	} else {
		friends.push(friend)
		res.status(201).send()
	}
})

friendRouter.get("/randomfriend", async (req: Request, res: Response) => {
	const response = await axios.get("http://api.namefake.com")
	const data = response.data
	res.json({ "username": data.username, "name": data.name, "email": `${data.email_u}@${data.email_d}` })
})

friendRouter.post("/randomfriend", async (req: Request, res: Response) => {
	const response = await axios.get("http://api.namefake.com")
	const data = response.data
	console.log({ "username": data.username, "name": data.name, "email": `${data.email_u}@${data.email_d}` })
	friends.push({ "username": data.username, "name": data.name, "email": `${data.email_u}@${data.email_d}` })
	res.status(201).send()
})

export default friendRouter