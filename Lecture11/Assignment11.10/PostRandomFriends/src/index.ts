import axios from "axios"

interface Response {
    username: string
    name: string
    email_u: string
    email_d: string
}

async function postRandomFriends(): Promise<void> {
    const response = await axios.get("http://api.namefake.com")
    const typedResult: Response = response.data
    axios.post("http://localhost:3000/", { "username": typedResult.username, "name": typedResult.name, "email": `${typedResult.email_d}@${typedResult.email_u}`})
    return new Promise<void>((resolve, reject) => { 
        console.log("new random friend posted", Date.now())
        setTimeout(() => {
            resolve()
        },10000)
    })
}

(async () => { while(true) {
    await postRandomFriends()
    }
})()