"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const eventRouter = express_1.default.Router();
const events = [];
eventRouter.get("/", (req, res) => {
    res.json(["2nd edition", events]);
});
eventRouter.get("/:monthNumber", (req, res) => {
    const monthNumber = Number(req.params.monthNumber);
    const eventInfo = events.filter(item => new Date(item.date).getMonth() === monthNumber - 1);
    res.json(eventInfo);
});
eventRouter.post("/", (req, res) => {
    const event = req.body;
    if (typeof event.id !== "number" || typeof event.title !== "string" || typeof event.description !== "string" || typeof event.date !== "string") {
        res.status(400).send("ERROR, missing parameter or wrong type");
    }
    else {
        events.push(event);
        res.status(201).send();
    }
});
eventRouter.put("/:eventId", (req, res) => {
    const id = Number(req.params.eventId);
    const event = req.body;
    const index = events.findIndex(item => item.id === id);
    if (index === -1) {
        res.status(404).send("ERROR, event id not found");
    }
    else {
        if (typeof event.title === "string") {
            events[index].title = event.title;
        }
        if (typeof event.description === "string") {
            events[index].description = event.description;
        }
        if (typeof event.date === "string") {
            events[index].date = event.date;
        }
        if (typeof event.time === "string") {
            events[index].time = event.time;
        }
        res.status(204).send();
    }
});
eventRouter.delete("/:eventId", (req, res) => {
    const id = Number(req.params.eventId);
    const index = events.findIndex(item => item.id === id);
    if (index === -1) {
        res.status(404).send("ERROR, event id not found");
    }
    else {
        events.splice(index, 1);
        res.status(204).send();
    }
});
exports.default = eventRouter;
