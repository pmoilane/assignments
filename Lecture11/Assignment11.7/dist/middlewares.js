"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.unknownEndpoint = exports.logger = void 0;
const logger = (req, res, next) => {
    const now = new Date();
    const method = req.method;
    const url = req.url;
    const body = req.body;
    if (!body) {
        console.log(now.toLocaleString(), method, url);
    }
    else {
        console.log(now.toLocaleString(), method, url, body);
    }
    next();
};
exports.logger = logger;
const unknownEndpoint = (_req, res) => {
    res.status(404).send("ERROR, Endpoint not valid");
};
exports.unknownEndpoint = unknownEndpoint;
