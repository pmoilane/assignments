import express, {Request, Response} from "express"

const eventRouter = express.Router()

interface Event {
	id: number,
	title: string,
	description: string,
	date: string,
	time?: number
}

const events: Array<Event> = []

eventRouter.get("/", (req: Request, res: Response) => {
	res.json(events)
})

eventRouter.get("/:monthNumber", (req: Request, res: Response) => {
	const monthNumber = Number(req.params.monthNumber)
	const eventInfo = events.filter(item => new Date(item.date).getMonth() === monthNumber - 1)
	res.json(eventInfo)
})

eventRouter.post("/", (req: Request, res: Response) => {
	const event = req.body
	if (typeof event.id !== "number" || typeof event.title !== "string" || typeof event.description !== "string" || typeof event.date !== "string") {
		res.status(400).send("ERROR, missing parameter or wrong type")
	} else {
		events.push(event)
		res.status(201).send()
	}
})

eventRouter.put("/:eventId", (req: Request, res: Response) => {
	const id = Number(req.params.eventId)
	const event = req.body
	const index = events.findIndex(item => item.id === id)
	if (index === -1) {
		res.status(404).send("ERROR, event id not found")
	} else {
		if (typeof event.title === "string") {
			events[index].title = event.title
		}
		if (typeof event.description === "string") {
			events[index].description = event.description
		}
		if (typeof event.date === "string") {
			events[index].date = event.date
		}
		if (typeof event.time === "string") {
			events[index].time = event.time
		}
		res.status(204).send()
	}
})

eventRouter.delete("/:eventId", (req: Request, res: Response) => {
	const id = Number(req.params.eventId)
	const index = events.findIndex(item => item.id === id)
	if (index === -1) {
		res.status(404).send("ERROR, event id not found")
	} else {
		events.splice(index, 1)
		res.status(204).send()
	}
})

export default eventRouter