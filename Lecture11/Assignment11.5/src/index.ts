import express, { Request, Response } from "express"

const server = express()

const PORT = process.env.PORT ?? 3000

server.listen(PORT, () => {
    console.log("Listening to port, ", 3000)
});

server.get("/", (_req: Request, res: Response) => {
    res.send("ok:3")
});