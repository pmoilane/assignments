import express from "express"
import { logger, unknownEndpoint } from "./middlewares"
import eventRouter from "./eventRouter"

const PORT = process.env.PORT ?? 3000

const server = express()
server.use(express.json())

server.use(logger)

server.use("/", eventRouter)

server.use(unknownEndpoint)

server.listen(PORT, () => {
    console.log("Listening to port, ", 3000)
})
