import express from "express"
import { logger, unknownEndpoint } from "./middlewares"
import eventRouter from "./eventRouter"

const server = express()
server.use(express.json())

server.use(express.static("public"))

server.use(logger)

server.use("/calendar", eventRouter)

server.use(unknownEndpoint)

server.listen(3000)
