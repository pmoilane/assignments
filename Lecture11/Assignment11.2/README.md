Downtime allowed before compensation: 1.AKS (Azure Kubernetes Service), 2. App Service with Free tier, Azure DNS

1. 262 minutes 59 seconds, <99.95%
2. Free tier will not be compensated
3. Any downtime is eligible for compensation <100%
